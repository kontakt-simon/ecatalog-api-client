# Klient PHP dla REST Api e-katalogu Kontakt-Simon #
Pakiet służy do komunikacji z API e-katalogu produktów Kontakt-Simon.

## Wymagania ##
PHP w wersji >= 5.6.0

## Instalacja ##
Preferowanym sposobem instalacji pakietu jest instalacja za pomocą [Composer'a](http://getcomposer.org/). *Composer* to narzędzie pozwalające na łatwe zarządzanie zależnościami w projekcie. Pozwala on deklarować różne zależności, których potrzebuje projekt oraz instalować lub aktualizować je automatycznie. Więcej informacji o używaniu *Composer'a* znajduje się [tutaj](https://getcomposer.org/doc/00-intro.md).

### Dodawanie zależności ###
W pliku *composer.json* należy dodać następujące zależności:

```
{
    "require" : {
        "kontakt-simon/ecatalog-api-client" : "~1.3"
    },
    "repositories" : [{
            "type" : "vcs",
            "url" : "https://bitbucket.org/kontakt-simon/ecatalog-api-client.git"
        }
    ]
}
```

Po zainstalowaniu pakietu komendą `composer.phar install` należy dołączyć w kodzie plik automatycznie ładujący klasy:


```
#!php
require 'vendor/autoload.php';
```

## Korzystanie z pakietu ##
Obiekt klienta należy utworzyć w sposób pokazany poniżej, używając prywatnego klucza API.

```
#!php
<?php

use KontaktSimon\Ecatalog\Api\v2\RestClient;

require_once __DIR__ . "/vendor/autoload.php";

define("API_KEY", "prywatny-klucz-api");

$client = new RestClient(API_KEY);
```

Po zainicjalizowaniu klienta można za jego pomocą wysyłać zapytania do API e-katalogu. Poniżej przykładowy sposób użycia:

```
#!php
<?php

...

$productId = 5924;

try {
    $product = $client->getProduct($productId);
    var_dump($product->toArray());
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    echo $e->getMessage(), PHP_EOL;
}
```

### Pobieranie danych produktu ###
Dane pojedynczego produktu można pobrać za pomocą metody `RestClient::getProduct($productId, boolean $raw = false)`, gdzie parametr `$productId` może być identyfikatorem produktu w ekatalogu (np. 4408), numerem EAN13 (np. 5902787822101) lub symbolem produktu (np. "DW1ZL.01/11"). Domyślnie metoda zwraca obiekt klasy [`Product`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Product.php?at=master&fileviewer=file-view-default). Jeśli jako parametr `$raw` zostanie przekazana wartość `true`, metoda zwróci jako wynik oryginalne dane w formacie json. W przypadku gdy produkt nie zostanie znaleziony metoda zwraca `null`.

### Pobieranie danych wielu produktów jednocześnie ###
Do pobierania danych wielu produktów służy metoda `RestClient::getProducts(array $productIdList, boolean $raw = false)`. Parametr `$productIdList` to tablica identyfikatorów produktów (id, ean lub symbol). Parametr `$raw` określa, czy dane maja być zwracane jako json czy jako obiekt kolekcji. W wyniku metoda zwraca obiekt zbiór obiektów [`Collection`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Collection.php?at=master&fileviewer=file-view-default) klasy [`Product`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Product.php?at=master&fileviewer=file-view-default). W przypadku gdy nie zostaną znalezione żadne produkty, metoda zwróci pustą kolekcję. Przykładowy kod:


```
#!php
<?php

...

$productIdList = [5924, 5902787557614, "DW1ZL.01/11"];

try {
    $products = $client->getProducts($productIdList);
    foreach ($products as $product) {
        echo $product->getName(), PHP_EOL;
    }

    // wyświetla całą kolekcję obiektów jako tablicę
    var_dump($products->toArray());

    // wybiera z kolecji produktu o okreslonym id
    var_dump($products->getById(5924)->toArray());

} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}
```

### Pobieranie drzewa kategorii produktów ###
Całe drzewo kategorii wraz z przypisanymi produkatami można pobrać za pomocą metody `RestClient::getCategoryTree(boolean $raw = false)`. Metoda zwraca obiekt klasy [`Category`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Category.php?at=master&fileviewer=file-view-default). Podkategorie danej kategorii zbznajdują się w kolekcji obiektów klasy [`Category`], można je pobrać za pomocą metody `Category::getSubCategories()`. Lista identyfikatorów produktów przypisanych do danej kategorii w postaci tablicy int dostępna jest za pomoca metody `Category::getArticleIdList()`. Jeśli jako parametr `$raw` zostanie przekazana wartość `true`, metoda zwróci jako wynik oryginalne dane w formacie json.
Uwaga: struktura kategorii oraz produkty przypisane do nich mogą być różne dla różnych wersji językowych. Wersję pobieranych danych można zmienić za pomocą metody `RestClient::setLanguage(string $lang)`.

Przykładowy kod pobierania danych (więcej w przykładach użycia na końcu dokumentu):

```
#!php
<?php

...

function printCategory(\KontaktSimon\Ecatalog\Api\v2\Model\Category $category) {
    echo str_repeat('  ', count($category->getPath())), '+ ', $category->getName(), PHP_EOL;
    if (!empty($category->getArticleIdList())) {
        echo str_repeat('  ', count($category->getPath())), '  produkty: [', join(', ', $category->getArticleIdList()), ']', PHP_EOL;
    }
    foreach ($category->getSubcategories() as $subCategory) {
        printCategory($subCategory);
    }
}

/* @var \KontaktSimon\Ecatalog\Api\v2\Model\Category $catalog */
$catalog = null;

try {
    $catalog = $client->getCategoryTree();
    printCategory($catalog);
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}

```

Rezultat wykonania:
```
+ KONTAKT-SIMON
  + Osprzęt elektroinstalacyjny
    + Simon 54
      + Ramki Simon 54 Nature (+ akcesoria do ramek)
        + Ramki 1- krotne Nature
          produkty: [10688, 1224, 10694, 10689, 10690, 1195, 10691, 1311, 1335, 1196, 10692, 10693, 1200, 1208, 1220, 11825, 11826, 11827, 11828, 11829, 11830]
        + Ramki 2- krotne Nature
          produkty: [10695, 1217, 10703, 10696, 10697, 1209, 10698, 1225, 1229, 10699, 10700, 10701, 10702, 1233, 1213, 11831, 11832, 11833, 11834, 11835, 11836]
        + Ramki 3- krotne Nature
          produkty: [10704, 1218, 10714, 10705, 10706, 10707, 10708, 1230, 1234, 10709, 10710, 10711, 10712, 10713, 1214, 11837, 11838, 11839, 11840, 11841, 11842]
        + Ramki 4- krotna Nature
          produkty: [10715, 1391, 10724, 10716, 10717, 10718, 10719, 1235, 1215, 10720, 10721, 10722, 10723, 1219, 1239, 11843, 11844, 11845, 11846, 11847, 11848]
        + Ramki 5- krotna Nature
          produkty: [10952, 10966, 10965, 10953, 10954, 10955, 10956, 10957, 10958, 10959, 10960, 10961, 10962, 10963, 10964, 11849, 11850, 11851, 11852, 11853, 11854]
        + Uszczelki IP44 do ramek Nature
          produkty: [10685, 1265, 10686, 10687, 10951]
      + Ramki Simon 54 Premium  (+ akcesoria do ramek)
        + Ramki 1- krotne Premium
          produkty: [4998, 4999, 5000, 12762, 5001, 5002, 5003, 5028, 12761, 5029, 5030, 5031, 5032, 5033, 5058, 5062, 5066, 5070, 5074]
...
```

### Pobieranie informacji o zmienionych/usuniętych produktach ###
Do pobierania danych o zmienionych luyb usunietych produktach służy metoda `RestClient::getUpdatedProducts(integer $timestamp)`. Parametr `$timestamp` określa czas, od którego mają być swórcone zmiany. Metoda zwraca tablicę z listą zmienionych oraz usuniętych identyfikatorów produktów. Przykładowy kod:

```
#!php
<?php

...

try {
    $modifiedProducts = $client->getUpdatedProducts(1479550422);
    var_dump($modifiedProducts["updatedArticles"]);
    var_dump($modifiedProducts["deletedArticles"]);
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}
```

### Pobieranie pliku obrazka produktu ###
Dane plików graficznych danego produktu reprezentuja obiekty typu [`File`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/41882a6d7e7236125b91355fdf81db6e701b9d03/src/KontaktSimon/Ecatalog/Api/v2/Model/Product/File.php?at=master&fileviewer=file-view-default). Obiekt klasy `Product` zawiera kolekcję dostepnych plików graficznych, którą mozna pobrać metodą `Product::getImages()`. Pojedynczy plik graficzny można pobrać za pomocą metody `RestClient::fetchProductImage($imageUrl, $destPath)`. Przykładowy kod:

```
#!php
<?php

...

$productId = 5924;

try {
    $product = $client->getProduct($productId);
    // pobierz pierwszy obrazek dla produktu i zapisz w katalogu images
    /* @var $image \KontaktSimon\Ecatalog\Api\v2\Model\Product\File */
    $image = $product->getImages()->current();
    $client->fetchProductImage($image->getUrl(), "images/{$image->getFileName()}");
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}
```

### Zmiana języka zwracanych wyników ###
Domyślnie wyniki zwracane są w języku polskim. Język można zmienić za pomoca metody `RestClient::setLanguage(string $lang)`. Listę dostepnych języków można pobrać za pomocą metody `RestClient::getAvailableLanguages()`.

```
#!php
<?php

...

// zmień domyślny język na angielski
$client->setLanguage("en-GB");

...
```

### Pobieranie informacji o limitach ###
Na klucz API może być nałożony dzienny limit zapytań. Stan ilości zapytań można sprawdzić wywołując metodę `RestClient::getStatus()`.

```
#!php
<?php

...

// pobierz dane o limicie zapytań
var_dump($client->getStatus());

...
```

W rezultacie otrzymamy:

```
#!php
Array
(
    [dailyLimit] => 10000
    [requestsLeft] => 9999
    [requestsToday] => 47
)
```

### Pobieranie informacji o stanach magazynowych ###
Stany magazynowe produktów można sprawdzić wywołując metodę `RestClient::getStockAvailability()`. Stany magazynowe aktualizowane są co godzinę.

```
#!php
<?php

...

// pobierz dane o stanach magazynowych
var_dump($client->getStockAvailability());

...
```

W rezultacie otrzymamy tablicę elementów, w której kluczem jest identyfikator produktu, wartością zaś dla danego identyfikatora symbol produktu oraz dostępna ilość:

```
#!php
Array
(
    [4592] => Array
        (
            [symbol] => DPZK.01/41
            [stockAvailability] => 120
        )
...
```

### Obsługa wyjątków ###
W razie błędu komunikacji z Api lub podania niewłaściwych danych, klient wyrzuci jeden z wyjątków implementujących interfejs `\KontaktSimon\Ecatalog\Api\RestClient\Exception`:

* `InvalidApiKeyException` - w przypadku gdy prywatny klucz API jest nieważny lub błędny,
* `InvalidDataReceivedException` - w przypadku gdy klient otrzyma błędne dane z serwera API,
* `NotFoundException` - w przypadku gdy klient odwołuje się do zasobu nieistniejącego na serwerze,
* `RequestRateExceededException` - w przypadku gdy przekroczono dozwoloną ilość dziennych zapytań; metoda `RequestRateExceededException::getMessage()` zwróci czas w sekundach do kolejnego możliwego zapytania,
* `ServerErrorException` - zostanie wyrzucony w przypadku błędu aplikacji serwera API.

### Przykłady użycia ###
**Iteracja po plikach dokumentów produktu:**

```
#!php
<?php

$product = $client->getProduct(5924);

/* @var $docGroup \KontaktSimon\Ecatalog\Api\v2\Model\Product\DocumentGroup */
foreach ($product->getDocuments() as $docGroup) {
    echo $docGroup->getName(), PHP_EOL, "------------------------------------------------", PHP_EOL;
    /* @var $doc \KontaktSimon\Ecatalog\Api\v2\Model\Product\Document */
    foreach ($docGroup->getDocuments() as $doc) {
        echo $doc->getName(), PHP_EOL;
        /* @var $file \KontaktSimon\Ecatalog\Api\v2\Model\Product\File*/
        foreach ($doc->getFiles() as $file) {
            echo $file->getUrl(), PHP_EOL;
        }
        echo PHP_EOL;
    }
}
```

Rezultat wykonania:
```
Deklaracja zgodności
------------------------------------------------

https://ekatalog.kontakt-simon.mobi/download/310/K-S_234D.pdf

Foldery, broszury
------------------------------------------------
Broszura Simon Basic
https://ekatalog.kontakt-simon.mobi/download/38/Broszura_Simon_Basic.pdf
```

**Iteracja po parametrach produktu**

```
#!php
<?php

$product = $r->getProduct(5924);

/* @var $param \KontaktSimon\Ecatalog\Api\v2\Model\Product\Param */
echo "<table><thead><tr><td>Nazwa parametru</td><td>Wartość</td><td>Opcjonalny opis</td></tr></thead><tbody>";
foreach ($product->getParams() as $param) {
    echo "<tr><td>{$param->getName()}</td><td>{$param->getValue()}</td><td>{$param->getDescription()}</td></tr>";
}
echo "</tbody></table>";
```