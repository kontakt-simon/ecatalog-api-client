<?php

namespace KontaktSimon\Ecatalog\Api\RestClient;

use GuzzleHttp\ClientInterface;
use KontaktSimon\Ecatalog\Api\RestClient\Exception\InvalidApiKeyException;
use KontaktSimon\Ecatalog\Api\RestClient\Exception\InvalidDataReceivedException;
use KontaktSimon\Ecatalog\Api\RestClient\Exception\NotFoundException;
use KontaktSimon\Ecatalog\Api\RestClient\Exception\ServerErrorException;

abstract class AbstractRestClient {

    const API_KEY_HEADER = "X-ApiKey";

    /**
     * Private api key
     * @var string
     */
    protected $apiKey;

    /**
     * Language for results
     * @var mixed
     */
    protected $language = null;

    /**
     * HTTP client
     *
     * @var ClientInterface
     */
    protected $client = null;

    /**
     * Last exception from client
     * @var Exception
     */
    protected $lastException = null;


    /**
     * Should return base url for api
     * @return string
     */
    abstract protected function getBaseUrl();


    /**
     * Create Ecatalog REST Api client
     * @param string $apiKey
     * @param ClientInterface $client
     */
    public function __construct($apiKey, ClientInterface $client = null) {
        $this->apiKey = $apiKey;
        if (null !== $client) {
            $this->client = $client;
        } else {
            $this->client = new \GuzzleHttp\Client([
                "base_uri" => $this->getBaseUrl()
            ]);
        }
    }

    /**
     * Set language for retrieving data
     * Set to null for default api language
     * @param string $lang
     */
    public function setLanguage($lang) {
        $this->language = $lang;
    }

    /**
     * Get language set for retrieved data
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * Get language list available in api
     * @return array
     */
    public function getAvailableLanguages() {
        return json_decode($this->request("GET", "catalog/languages.json"), true);
    }

    /**
     * Get list of changed/deleted products since timestamp
     * @integer $timestamp
     * @return array
     */
    public function getUpdatedProducts($timestamp) {
        return json_decode($this->request("GET", "catalog/changes.json", ["from" => $timestamp]), true);
    }

    /**
     * Get request status
     * @return array
     */
    public function getStatus() {
        return json_decode($this->request("GET", "status.json"), true);
    }

    /**
     * Get stock availability as array
     * @return array
     */
    public function getStockAvailability() {
        return json_decode($this->request("GET", "catalog/stocks.json"), true);
    }

    /**
     * Get last exception from client
     * @return \KontaktSimon\Ecatalog\Api\RestClient\Exception
     */
    public function getLastException() {
        return $this->lastException;
    }

    /**
     * Make HTTP request
     * @param string $method
     * @param string $path
     * @param array $queryParams
     * @param boolean $validateJsonResponse
     * @param array $additionalClientOptions
     * @return string
     */
    protected function request($method, $path, array $queryParams = [], $validateJsonResponse = true, array $additionalClientOptions = []) {
        $response = null;
        $this->lastException = null;

        if ($this->getLanguage() != null) {
            $queryParams["lang"] = $this->getLanguage();
        }
        $params = !empty($queryParams) ? "?" . http_build_query($queryParams) : "";

        try {
            $response = $this->client->request($method, $path . $params, ["headers" => [self::API_KEY_HEADER => $this->apiKey]] + $additionalClientOptions);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody());
            if (is_null($responseData)) {
                $responseData = new \stdClass();
                $responseData->message = $e->getMessage();
            }

            switch ($response->getStatusCode()) {
                case 401: {
                    $this->lastException = new InvalidApiKeyException($responseData->message, $response->getStatusCode(), $e);
                } break;

                case 404: {
                    $this->lastException = new NotFoundException($responseData->message, $response->getStatusCode(), $e);
                } break;

                default: {
                    $this->lastException = new ServerErrorException($responseData->message, $response->getStatusCode(), $e);
                } break;
            }

            throw $this->lastException;
        }
        if ($validateJsonResponse && !json_decode((string) $response->getBody())) {
            throw new InvalidDataReceivedException("Invalid JSON data received");
        }
        return (string) $response->getBody();
    }
}