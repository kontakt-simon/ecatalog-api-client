<?php

namespace KontaktSimon\Ecatalog\Api\RestClient\Exception;

class ServerErrorException extends \RuntimeException implements \KontaktSimon\Ecatalog\Api\RestClient\Exception {}