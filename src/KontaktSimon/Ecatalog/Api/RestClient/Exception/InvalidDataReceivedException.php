<?php

namespace KontaktSimon\Ecatalog\Api\RestClient\Exception;

class InvalidDataReceivedException extends \RuntimeException implements \KontaktSimon\Ecatalog\Api\RestClient\Exception {}