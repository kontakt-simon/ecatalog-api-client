<?php

namespace KontaktSimon\Ecatalog\Api\RestClient\Exception;

class NotFoundException extends \RuntimeException implements \KontaktSimon\Ecatalog\Api\RestClient\Exception {}