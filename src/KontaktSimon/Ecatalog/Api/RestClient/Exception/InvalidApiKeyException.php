<?php

namespace KontaktSimon\Ecatalog\Api\RestClient\Exception;

class InvalidApiKeyException extends \RuntimeException implements \KontaktSimon\Ecatalog\Api\RestClient\Exception {}