<?php

namespace KontaktSimon\Ecatalog\Api\v2\Model\Product;

use KontaktSimon\Ecatalog\Api\v2\Model\Collection;

class Document {

    /**
     * @var string
     */
    private $name;

    /**
     * @var Collection<File>
     */
    private $files;


    /**
     * Private constructor
     */
    private function __construct() {}

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return \KontaktSimon\Ecatalog\Api\v2\Model\Collection
     */
    public function getFiles() {
        return $this->files;
    }

    /**
     * Create new param object from json data
     * @param string $json
     * @return Param
     */
    public static function fromJson($json) {
        return self::fromArray(json_decode($json, true));
    }

    /**
     * Create new document object from array
     * @param array $data
     * @return Param
     */
    public static function fromArray(array $data) {
        $p = new self();
        $p->name = $data["name"];
        $p->files = new Collection('KontaktSimon\Ecatalog\Api\v2\Model\Product\File');
        foreach ($data["files"] as $file) {
            $p->files->add(File::fromArray($file));
        }

        return $p;
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            "name" => $this->getName(),
            "files" => $this->getFiles()->toArray()
        ];
    }

    /**
     * @return string
     */
    public function toJson() {
        return json_encode($this->toArray());
    }

}