<?php

namespace KontaktSimon\Ecatalog\Api\v2\Model\Product;

class File {

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $md5;


    /**
     * Private constructor
     */
    private function __construct() {}

    /**
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getMd5() {
        return $this->md5;
    }

    /**
     * @return string
     */
    public function getFileName() {
        return basename($this->getUrl());
    }

    /**
     * Create new param object from json data
     * @param string $json
     * @return Param
     */
    public static function fromJson($json) {
        return self::fromArray(json_decode($json, true));
    }

    /**
     * Create new file object from array
     * @param array $data
     * @return Param
     */
    public static function fromArray(array $data) {
        $p = new self();
        $p->url = $data["url"];
        $p->md5 = $data["md5"];
        return $p;
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            "url" => $this->getUrl(),
            "fileName" => $this->getFileName(),
            "md5" => $this->getMd5()
        ];
    }

    /**
     * @return string
     */
    public function toJson() {
        return json_encode($this->toArray());
    }

}