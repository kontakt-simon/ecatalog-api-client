<?php

namespace KontaktSimon\Ecatalog\Api\v2\Model\Product;

use KontaktSimon\Ecatalog\Api\v2\Model\Collection;

class DocumentGroup {

    /**
     * Group id
     * @var integer
     */
    private $id;

    /**
     * Group name
     * @var string
     */
    private $name;

    /**
     * Documents
     * @var Collection
     */
    private $documents;


    /**
     * Private constructor
     */
    private function __construct() {}

    /**
     * Get parameter id
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get parameter name
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get files
     * @return Collection
     */
    public function getDocuments() {
        return $this->documents;
    }

    /**
     * Create new document group object from json data
     * @param string $json
     * @return Param
     */
    public static function fromJson($json) {
        return self::fromArray(json_decode($json, true));
    }

    /**
     * Create new document group object from array
     * @param array $data
     * @return Param
     */
    public static function fromArray(array $data) {
        $p = new self();
        $p->id = $data["groupId"];
        $p->name = $data["groupName"];
        $p->documents = new Collection('KontaktSimon\Ecatalog\Api\v2\Model\Product\Document');
        foreach ($data["documents"] as $doc) {
            $p->documents->add(Document::fromArray($doc));
        }

        return $p;
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "documents" => $this->getDocuments()->toArray()
        ];
    }

    /**
     * @return string
     */
    public function toJson() {
        return json_encode($this->toArray());
    }

}