<?php

namespace KontaktSimon\Ecatalog\Api\v2\Model\Product;


class Param {

    /**
     * Parameter id
     * @var integer
     */
    private $id;

    /**
     * Parameter name
     * @var string
     */
    private $name;

    /**
     * Parameter description
     * @var string
     */
    private $description;

    /**
     * Parameter value
     * @var string
     */
    private $value;

    /**
     * Parameter raw value
     * @var mixed
     */
    private $rawValue;

    /**
     * Parameter raw unit
     * @var string
     */
    private $rawUnit;


    /**
     * Private constructor
     */
    private function __construct() {}

    /**
     * Get parameter id
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get parameter name
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get parameter additional description
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Returns parameter value
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Get parameter raw value without unit
     * May return array with min and max keys if value is a range
     * @return mixed
     */
    public function getRawValue() {
        return $this->rawValue;
    }

    /**
     * Get parameter unit
     * @return string
     */
    public function getRawUnit() {
        return $this->rawUnit;
    }

    /**
     * Chack if parameter is scalar or range value
     * @return boolean
     */
    public function isRangeValue() {
        return is_array($this->rawValue);
    }

    /**
     * Create new param object from json data
     * @param string $json
     * @return Param
     */
    public static function fromJson($json) {
        return self::fromArray(json_decode($json, true));
    }

    /**
     * Create new param object from array
     * @param array $data
     * @return Param
     */
    public static function fromArray(array $data) {
        $p = new self();
        $p->id = $data["id"];
        $p->name = $data["name"];
        isset($data["description"]) and$p->description = $data["description"];
        $p->value = $data["value"];
        isset($data["rawValue"]) and $p->rawValue = $data["rawValue"];
        isset($data["rawUnit"]) and $p->rawUnit = $data["rawUnit"];
        return $p;
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "description" => $this->getDescription(),
            "value" => $this->getValue(),
            "rawValue" => $this->getRawValue(),
            "rawUnit" => $this->getRawUnit()
        ];
    }

    /**
     * @return string
     */
    public function toJson() {
        return json_encode($this->toArray());
    }
}