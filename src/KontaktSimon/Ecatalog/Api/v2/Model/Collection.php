<?php

/* $Id$ */

namespace KontaktSimon\Ecatalog\Api\v2\Model;

class Collection implements \Iterator {

    /**
     * Entities in collection
     * @var array
     */
    protected $_items = array();

    /**
     * Entities counter
     * @var integer
     */
    protected $_total = 0;

    /**
     * Pointer to current item
     * @var integer
     */
    protected $_current = 0;

    /**
     * @var string
     */
    protected $itemClassName = "";

    /**
     * Entities in collection
     * @var array
     */
    protected $_idLookup = array();


    /**
     * @param string $itemClassName
     */
    public function __construct($itemClassName = null) {
        if ($itemClassName) {
            $this->itemClassName = (string) $itemClassName;
        }
    }


    /**
     * Get object from entitiest or create new one
     *
     * @param integer $number
     * @return mixed
     */
    protected function _getObject($number) {
        $item = null;
        if (isset($this->_items[$number])) {
            $item = $this->_items[$number];
        }
        return $item;
    }


    /**
     * Returns valid item class name for Collection
     * Overload this method to return class name so only objects of that class could by added to collection
     *
     * @return string
     */
    protected function _getItemClassName() {
        return $this->itemClassName;
    }


    /**
     * Check whether we could store given type of object using this DAO
     *
     * @param mixed $item
     * @return boolean
     */
    protected function _isItemValid($item) {
        $className = $this->_getItemClassName();
        return empty($className) ? true : $item instanceof $className;
    }


    /**
     * Add item to collection
     *
     * @param mixed $item
     * @return mixed New collection item count
     * @throws \InvalidArgumentException
     */
    public function add($item) {
        if (!$this->_isItemValid($item)) {
            throw new \InvalidArgumentException("Item is not instance of " . $this->_getItemClassName() . " class");
        }

        if (method_exists($item, "getId")) {
            if (isset($this->_idLookup[$item->getId()])) {
                return false;
            }
            $this->_items[$this->_total] = $item;
            $position = $this->_total++;
            $this->_idLookup[$item->getId()] = $position;
            return $position;
        } else {
            $this->_items[$this->_total] = $item;
            return $this->_total++;
        }
    }


    /**
     * Inject array of objects
     *
     * @param array $items
     * @return integer New collection item count
     */
    public function inject(array $items) {
        foreach ($items as $item) {
            $this->add($item);
        }
        return $this->_total;
    }


    /**
     * @see \Iterator::rewind()
     */
    public function rewind() {
        $this->_current = 0;
    }


    /**
     * @see \Iterator::current()
     */
    public function current() {
        return $this->_getObject($this->_current);
    }


    /**
     * @see \Iterator::key()
     */
    public function key() {
        return $this->_current;
    }


    /**
     * @see \Iterator::next()
     */
    public function next() {
        $row = $this->_getObject($this->_current);
        if ($row) {
            $this->_current++;
        }
        return $row;
    }


    /**
     * @see \Iterator::valid()
     */
    public function valid() {
        return (!is_null($this->current()));
    }


    /**
     * Return collection as array
     *
     * @return array
     */
    public function toArray() {
        $i = 0;
        $result = array();
        while ($i < $this->_total) {
            $obj = $this->_getObject($i);
            if (method_exists($obj, "toArray")) {
                array_push($result, $obj->toArray());
            }
            $i++;
        }
        return $result;
    }

    /**
     * Return collection as Json serialized string
     *
     * @return string
     */
    public function toJson() {
        return json_encode($this->toArray());
    }


    /**
     * Return element count
     *
     * @return integer
     */
    public function count() {
        return $this->_total;
    }


    /**
     * Is collection empty
     *
     * @return boolean
     */
    public function isEmpty() {
        return $this->_total == 0;
    }

    /**
     * Collect all values for $key if object is array or implements toArray() and has such key
     *
     * @param string $key
     * @return array
     */
    public function collectValuesForKey($key) {
        $result = array();
        foreach ($this as $item) {
            if (is_object($item) && method_exists($item, "toArray")) {
                $item = $item->toArray();
            }
            if (is_array($item) && isset($item[$key])) {
                $result[] = $item[$key];
            }
        }

        return array_unique($result);
    }

    /**
     * Get entity by id
     *
     * @param integer $id
     */
    public function getById($id) {
        $id = (integer) $id;
        return isset($this->_idLookup[$id])
            ? $this->_getObject($this->_idLookup[$id])
            : null;
    }


    /**
     * Get id list of all objects in collection
     *
     * @return array<integer>
     */
    public function getIdList() {
        return array_keys($this->_idLookup);
    }
}