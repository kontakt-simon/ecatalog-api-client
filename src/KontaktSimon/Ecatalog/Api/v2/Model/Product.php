<?php

namespace KontaktSimon\Ecatalog\Api\v2\Model;

use KontaktSimon\Ecatalog\Api\v2\Model\Product\Param;
use KontaktSimon\Ecatalog\Api\v2\Model\Product\File;
use KontaktSimon\Ecatalog\Api\v2\Model\Product\DocumentGroup;

class Product {

    /**
     * Product id
     * @var integer
     */
    private $id;

    /**
     * Product symbol
     * @var string
     */
    private $symbol;

    /**
     * EAN13 number
     * @var integer
     */
    private $ean;

    /**
     * Product name
     * @var string
     */
    private $name;

    /**
     * Product price
     * @var float
     */
    private $price;

    /**
     * Price currency
     * @var string
     */
    private $currency;

    /**
     * Product description
     * @var string
     */
    private $description;

    /**
     * Product parameters
     * @var \KontaktSimon\Ecatalog\Api\v2\Model\Collection
     */
    private $params;

    /**
     * List of product images
     * @var \KontaktSimon\Ecatalog\Api\v2\Model\Collection
     */
    private $images;

    /**
     * List of attached documents
     * @var \KontaktSimon\Ecatalog\Api\v2\Model\Collection
     */
    private $documents;

    /**
     * Last modified timestamp
     * @var integer
     */
    private $lastModified;

    /**
     * E-catalog canonical url
     * @var string
     */
    private $canonicalUrl;

    /**
     * HTML parameters table
     * @var string
     */
    private $htmlParamsTable;

    /**
     * @var int
     */
    private $stockAvailability = 0;


    /**
     * Private constructor
     */
    private function __construct() {}

    /**
     * Create new product object from json data
     * @param string $json
     * @return Product
     */
    public static function fromJson($json) {
        return self::fromArray(json_decode($json, true));
    }

    /**
     * Create new product object from array data
     * @param array $data
     * @return Product
     */
    public static function fromArray(array $data) {
        $p = new self();
        $p->id = (int) $data["id"];
        $p->ean = $data["ean"];
        $p->symbol = $data["symbol"];
        $p->name = $data["name"];
        $p->description = $data["description"];
        $p->lastModified = $data["lastModified"];
        $p->canonicalUrl = $data["canonicalUrl"];
        $p->htmlParamsTable = $data["paramsHtml"];
        $p->price = $data["price"];
        $p->currency = $data["currency"];
        if (isset($data["stockAvailability"])) {
            $p->stockAvailability = (int) $data["stockAvailability"];
        }
        $p->params = new Collection('KontaktSimon\Ecatalog\Api\v2\Model\Product\Param');
        foreach ($data["params"] as $param) {
            $p->params->add(Param::fromArray($param));
        }

        $p->documents = new Collection('KontaktSimon\Ecatalog\Api\v2\Model\Product\DocumentGroup');
        foreach ($data["documents"] as $docGroup) {
            $p->documents->add(DocumentGroup::fromArray($docGroup));
        }

        $p->images = new Collection('KontaktSimon\Ecatalog\Api\v2\Model\Product\File');
        foreach ($data["images"] as $image) {
            $p->images->add(File::fromArray($image));
        }
        return $p;
    }

    /**
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSymbol() {
        return $this->symbol;
    }

    /**
     * @return integer
     */
    public function getEan() {
        return $this->ean;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getStockAvailability() {
        return $this->stockAvailability;
    }

    /**
     * @return string
     */
    public function getCurrency() {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return Collection<Param>
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * @return Collection<File>
     */
    public function getImages() {
        return $this->images;
    }

    /**
     * @return Collection
     */
    public function getDocuments() {
        return $this->documents;
    }

    /**
     * @return integer
     */
    public function getLastModified() {
        return $this->lastModified;
    }

    /**
     * @return string
     */
    public function getEcatalogCanonicalUrl() {
        return $this->canonicalUrl;
    }

    /**
     * @return string
     */
    public function getHtmlParamsTable() {
        return $this->htmlParamsTable;
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            "id" => $this->getId(),
            "symbol" => $this->getSymbol(),
            "ean" => $this->getEan(),
            "name" => $this->getName(),
            "price" => [
                'value' => $this->getPrice(),
                "currency" => $this->getCurrency(),
            ],
            "stockAvailability" => $this->getStockAvailability(),
            "description" => $this->getDescription(),
            "lastModified" => $this->getLastModified(),
            "canonicalUrl" => $this->getEcatalogCanonicalUrl(),
            "params" => $this->getParams()->toArray(),
            "images" => $this->getImages()->toArray(),
            "documents" => $this->getDocuments()->toArray()
        ];
    }

    /**
     * @return string
     */
    public function toJson() {
        return json_encode($this->toArray());
    }

}