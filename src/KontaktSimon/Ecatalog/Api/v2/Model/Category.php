<?php

namespace KontaktSimon\Ecatalog\Api\v2\Model;


class Category {

    /**
     * Category id
     * @var integer
     */
    private $id;

    /**
     * Category name
     * @var string
     */
    private $name;

    /**
     * Parent category Id
     * @var integer
     */
    private $parentId;

    /**
     * Subcategories
     * @var Collection
     */
    private $subCategories;

    /**
     * Product id list
     * @var int[]
     */
    private $articleIdList;

    /**
     * Category Path
     * @var int[]
     */
    private $path;


    /**
     * Private constructor
     */
    private function __construct() {}

    /**
     * Create new category object from json data
     * @param string $json
     * @return Product
     */
    public static function fromJson($json) {
        return self::fromArray(json_decode($json, true));
    }

    /**
     * Create new product object from array data
     * @param array $data
     * @return Product
     */
    public static function fromArray(array $data) {
        $p = new self();
        $p->id = $data["id"];
        $p->parentId = $data["parentId"];
        $p->name = $data["name"];
        $p->path = $data["path"];
        isset($data["articleIdList"]) && $p->articleIdList = $data["articleIdList"];
        $p->subCategories = new Collection('KontaktSimon\Ecatalog\Api\v2\Model\Category');
        if (isset($data["subCategories"])) {
            foreach ($data["subCategories"] as $cat) {
                $p->subCategories->add(self::fromArray($cat));
            }
        }
        return $p;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getParentId() {
        return $this->parentId;
    }

    /**
     * @return \KontaktSimon\Ecatalog\Api\v2\Model\Collection
     */
    public function getSubCategories() {
        return $this->subCategories;
    }

    /**
     * @return int[]
     */
    public function getArticleIdList() {
        return $this->articleIdList;
    }

    /**
     * @return int[]
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            "id" => $this->getId(),
            "parentId" => $this->getParentId(),
            "path" => $this->getPath(),
            "name" => $this->getName(),
            "subCategories" => $this->getSubCategories()->toArray(),
            "articleIdList" => $this->getArticleIdList()
        ];
    }

    /**
     * @return string
     */
    public function toJson() {
        return json_encode($this->toArray());
    }

}