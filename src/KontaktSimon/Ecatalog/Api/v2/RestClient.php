<?php

namespace KontaktSimon\Ecatalog\Api\v2;

use KontaktSimon\Ecatalog\Api\RestClient\AbstractRestClient;
use KontaktSimon\Ecatalog\Api\v2\Model\Category;
use KontaktSimon\Ecatalog\Api\v2\Model\Product;
use KontaktSimon\Ecatalog\Api\RestClient\Exception\NotFoundException;
use KontaktSimon\Ecatalog\Api\v2\Model\Collection;

class RestClient extends AbstractRestClient {

    /**
     * {@inheritDoc}
     * @see \KontaktSimon\Ecatalog\Api\RestClient\AbstractRestClient::getBaseUrl()
     */
    protected function getBaseUrl() {
        return "https://ekatalog.kontakt-simon.com.pl/api/v2/";
    }

    /**
     * @param string $id
     * @return string
     */
    protected function normalizeId($id) {
        return str_replace("/", "_", $id);
    }

    /**
     * Get single product from ecatalog
     * Id param may be product id, ean or symbol
     * @param integer|string $id
     * @param boolean $returnRaw returns json string if set to true
     * @return Product or null if not found
     */
    public function getProduct($id, $returnRaw = false) {
        if (!is_scalar($id)) {
            throw new \InvalidArgumentException("Product id should be scalar value");
        }

        $id = $this->normalizeId((string) $id);
        $data = null;
        try {
            $data = $this->request("GET", "catalog/article/{$id}.json");
        } catch (NotFoundException $e) {
            return null;
        }

        return (bool) $returnRaw ? $data : Product::fromJson($data);
    }

    /**
     * Get many products from ecatalog
     * Id param may be product id, ean or symbol
     * @param integer|string $id
     * @param boolean $returnRaw returns json string if set to true
     * @return Product or null if not found
     */
    public function getProducts(array $idList, $returnRaw = false) {
        $idListStr = "";
        foreach ($idList as $id) {
            if (!is_scalar($id)) {
                throw new \InvalidArgumentException("Product id should be scalar value");
            }
            $idListStr .= "," . $this->normalizeId((string) $id);
        }
        $idListStr = trim($idListStr, ",");
        $data = null;
        try {
            $data = $this->request("GET", "catalog/article/{$idListStr}.json");
        } catch (NotFoundException $e) {
            return null;
        }

        if ((bool) $returnRaw) {
            return $data;
        }

        $data = json_decode($data, true);
        $collection = new Collection('KontaktSimon\Ecatalog\Api\v2\Model\Product');
        if (isset($data["collection"]["items"])) {
            foreach ($data["collection"]["items"] as $item) {
                $collection->add(Product::fromArray($item));
            }
        } else {
            $collection->add(Product::fromArray($data));
        }

        return $collection;
    }

    /**
     * Fetch product image
     * @param string $imageUrl
     * @param string $destPath path to save file
     */
    public function fetchProductImage($imageUrl, $destPath) {
        $dir = pathinfo($destPath, PATHINFO_DIRNAME);
        if (!(is_dir($dir) && is_writable($dir))) {
            throw new \InvalidArgumentException("Destination path {$dir} is not writable or does not exist");
        }
        $this->request("GET", $imageUrl, [], false, ["save_to" => $destPath]);
    }


    /**
     * Get catalog category tree
     * @param boolean $returnRaw returns json string if set to true
     * @return Category or null if not found
     */
    public function getCategoryTree($returnRaw = false) {

        $data = null;
        try {
            $data = $this->request("GET", "catalog/category/tree.json");
        } catch (NotFoundException $e) {
            return null;
        }

        return (bool) $returnRaw ? $data : Category::fromJson($data);
    }

}