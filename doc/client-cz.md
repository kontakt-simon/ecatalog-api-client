# Klient PHP pro REST Api e-katalogu Kontakt-Simon  #
Paket slouží ke komunikaci s API e-katalogu produktů Kontakt-Simon.

## Požadavky ##
PHP ve verzi >= 5.6.0

## Instalace ##
Preferovaným způsobem instalace paketu je instalace s pomocí [Composeru](http://getcomposer.org/). *Composer* je nástroj umožňující snadnou správu závislostí v projektu. Umožňuje deklarovat různé závislosti, které projekt potřebuje, a automaticky je instalovat nebo aktualizovat. Více informací o používání *Composeru* [najdete zde](https://getcomposer.org/doc/00-intro.md).

### Přidávání závislostí ###
V souboru *composer.json* je třeba přidat následující závislosti:

```
{
    "require" : {
        "kontakt-simon/ecatalog-api-client" : "~1.3"
    },
    "repositories" : [{
            "type" : "vcs",
            "url" : "https://bitbucket.org/kontakt-simon/ecatalog-api-client.git"
        }
    ]
}
```

Po nainstalování paketu příkazem `composer.phar install` přidejte do kódu soubor automaticky nahrávající třídy:

```
#!php
require 'vendor/autoload.php';
```

## Používání paketu ##
Objekt klient vytvořte níže předvedeným způsobem, použijte přitom soukromý klíč API:

```
#!php
<?php

use KontaktSimon\Ecatalog\Api\v2\RestClient;

require_once __DIR__ . "/vendor/autoload.php";

define("API_KEY", "your private api key");

$client = new RestClient(API_KEY);
```

Po iniciování klienta lze s jeho pomocí zasílat dotazy do API e-katalogu. Níže příklad použití:

```
#!php
<?php

...

$productId = 5924;

try {
    $product = $client->getProduct($productId);
    var_dump($product->toArray());
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    echo $e->getMessage(), PHP_EOL;
}
```

### Stahování údajů produktu ###
Údaje jednoho produktu lze stáhnout s pomocí metody `RestClient::getProduct($productId, boolean $raw = false)`, kde parametr `$productId` může být identifikátor produktu v ekatalogu (např. 4408), číslo EAN13 (např. 5902787822101) nebo symbol produktu (např. "DW1ZL.01/11"). Ve výchozím nastavení metoda upozorňuje na objekt třídy  [`Product`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Product.php?at=master&fileviewer=file-view-default). Pokud bude jako parametr `$raw` předána hodnota true, metoda jako výsledek přinese originální údaje ve formátu json. V případě, kdy produkt nebude nalezen, metoda uvede výsledek `null`.

### Stahování údajů mnoha produktů najednou ###
K stahování údajů mnoha produktů slouží metoda `RestClient::getProducts(array $productIdList, boolean $raw = false)`. Parametr `$productIdList` je tabulka identifikátorů produktů (id, ean nebo symbol). Parametr `$raw` určuje, jestli údaje mají být zasílány zpět jako json nebo jako objekt kolekce. V důsledku metoda jako výsledek uvádí objekt sbírku objektů [`Collection`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Collection.php?at=master&fileviewer=file-view-default) třídy [`Product`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Product.php?at=master&fileviewer=file-view-default). V případě, kdy nebudou nalezeny žádné produkty, metoda jako výsledek přinese prázdnou kolekci. Příklad kódu:

```
#!php
<?php

...

$productIdList = [5924, 5902787557614, "DW1ZL.01/11"];

try {
    $products = $client->getProducts($productIdList);
    foreach ($products as $product) {
        echo $product->getName(), PHP_EOL;
    }

    // zobrazuje celou kolekci objektů jako tabulku
    var_dump($products->toArray());

    // vybírá z kolekce produkt s určitým id
    var_dump($products->getById(5924)->toArray());

} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}
```

### Stahování adresářového stromu kategorií produktů ###
Celý adresářový strom společně s přiřazenými produkty lze stáhnout s pomocí metody `RestClient::getCategoryTree(boolean $raw = false)`. Metoda jako výsledek přináší objekt třídy Category. Podkategorie dané kategorie se nacházejí v kolekci objektů třídy  [`Category`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Category.php?at=master&fileviewer=file-view-default), lze je stáhnout s pomocí metody `Category::getSubCategories()`. Seznam identifikátorů produktů přiřazených k dané kategorii v podobě tabulky int je dostupný s pomocí metody `Category::getArticleIdList()`. Pokud bude jako parametr `$raw` předána hodnota `true`, metoda jako výsledek přinese originální údaje ve formátu json. Pozor: struktura kategorií a k nim přiřazené produkty se mohou v různých jazykových verzích lišit. Verzi stahovaných dat lze změnit s pomocí metody `RestClient::setLanguage(string $lang)`.

Příklad kódu stahování dat (více v příkladech použití na konci dokumentu):

```
#!php
<?php

...

function printCategory(\KontaktSimon\Ecatalog\Api\v2\Model\Category $category) {
    echo str_repeat('  ', count($category->getPath())), '+ ', $category->getName(), PHP_EOL;
    if (!empty($category->getArticleIdList())) {
        echo str_repeat('  ', count($category->getPath())), '  produkty: [', join(', ', $category->getArticleIdList()), ']', PHP_EOL;
    }
    foreach ($category->getSubcategories() as $subCategory) {
        printCategory($subCategory);
    }
}

/* @var \KontaktSimon\Ecatalog\Api\v2\Model\Category $catalog */
$catalog = null;

try {
    $catalog = $client->getCategoryTree();
    printCategory($catalog);
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}

```

Výsledek provedení:
```
+ KONTAKT-SIMON
  + Elektroinstalační materiál
    + Simon 82 Detail / Nature / Sense
      + Rámečky Simon 82 Detail ORIGINAL + příslušenství
        + Rámečky 1-násobne Detail ORIGINAL
          produkty: [13356, 13364, 13360, 13400, 13404, 13368, 13408, 13412, 13416, 13420, 13372, 13376, 13380, 13384, 13388, 13392, 13424, 13396, 13428]
        + Rámečky 2-násobne Detail ORIGINAL
          produkty: [13357, 13365, 13361, 13401, 13405, 13369, 13409, 13413, 13417, 13421, 13373, 13377, 13381, 13385, 13389, 13393, 13425, 13397, 13429]
        + Rámečky 3-násobne Detail ORIGINAL
          produkty: [13358, 13366, 13362, 13402, 13406, 13370, 13410, 13414, 13418, 13422, 13374, 13378, 13382, 13386, 13390, 13394, 13426, 13398, 13430]
        + Rámečky 4-násobne Detail ORIGINAL
          produkty: [13359, 13367, 13363, 13403, 13407, 13371, 13411, 13415, 13419, 13423, 13375, 13379, 13383, 13387, 13391, 13395, 13427, 13399, 13431]
        + Těsnění pro rámečky Simon 82 Detail
          produkty: [11857, 11858, 11859, 11860]
...
```

### Stahování informací o změněných/odstraněných produktech ###
K stahování dat o změněných nebo odstraněných produktech slouží metoda `RestClient::getUpdatedProducts(integer $timestamp)`. Parametr `$timestamp` určuje čas, od kdy mají být změny nalézány jako výsledek. Metoda jako výsledek přináší tabulku se seznamem změněných a odstraněných identifikátorů produktů. Příklad kódu:

```
#!php
<?php

...

try {
    $modifiedProducts = $client->getUpdatedProducts(1479550422);
    var_dump($modifiedProducts["updatedArticles"]);
    var_dump($modifiedProducts["deletedArticles"]);
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}
```

### Stahování souboru obrázek produktu ###
Údaje grafických souborů daného produktu reprezentují objekty typu  [`File`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/41882a6d7e7236125b91355fdf81db6e701b9d03/src/KontaktSimon/Ecatalog/Api/v2/Model/Product/File.php?at=master&fileviewer=file-view-default). Objekt třídy `Product` obsahuje kolekci dostupných grafických souborů, které lze stáhnout metodou `Product::getImages()`. Jednotlivý grafický soubor lze stáhnout s pomocí metody `RestClient::fetchProductImage($imageUrl, $destPath)`. Příklad kódu:

```
#!php
<?php

...

$productId = 5924;

try {
    $product = $client->getProduct($productId);
    // stáhnout první obrázek pro produkt a uložit ho v katalogu images
    /* @var $image \KontaktSimon\Ecatalog\Api\v2\Model\Product\File */
    $image = $product->getImages()->current();
    $client->fetchProductImage($image->getUrl(), "images/{$image->getFileName()}");
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}
```

### Změna jazyka uvádění výsledků ###
Výchozí výsledky jsou uváděny v polštině. Jazyk lze změnit s pomocí metody `RestClient::setLanguage(string $lang)`. Seznam dostupných jazyků lze stáhnout s pomocí metody `RestClient::getAvailableLanguages()`.

```
#!php
<?php

...

// změnit výchozí jazyk na český
$client->setLanguage("cs-CZ");

...
```

### Stahování informací o limitech ###
Na klíč API může být uvalen denní limit dotazů. Stav počtu dotazů lze zkontrolovat vyvoláním metody `RestClient::getStatus()`.

```
#!php
<?php

...

// stáhnout údaje o limitu dotazů
var_dump($client->getStatus());

...
```

Ve výsledku získáme:

```
#!php
Array
(
    [dailyLimit] => 10000
    [requestsLeft] => 9999
    [requestsToday] => 47
)
```

### Stahování informací o stavu skladových zásob ###
Skladové stavy produktů lze ověřovat vyvoláním metody `RestClient::getStockAvailability()`. Skladové stavy jsou aktualizovány každou hodinu.

```
#!php
<?php

...

// stáhnout údaje o skladových stavech
var_dump($client->getStockAvailability());

...
```

Ve výsledku získáme tabulku prvků, ve které je klíčem identifikátor produktu, hodnotou je pak pro daný identifikátor symbol produktu a dostupné množství:

```
#!php
Array
(
    [4592] => Array
        (
            [symbol] => DPZK.01/41
            [stockAvailability] => 120
        )
...
```

### Obsluha výjimek ###
V případě chyby komunikace s Api nebo uvedení nesprávných údajů klient vypustí jednu z výjimek implementujících rozhraní `\KontaktSimon\Ecatalog\Api\RestClient\Exception`:

* `InvalidApiKeyException` - v případě, kdy je soukromý klíč API neplatný nebo chybný,
* `InvalidDataReceivedException` - v případě, kdy klient dostane chybné údaje ze serveru API,
* `NotFoundException` - v případě, kdy se klient odkazuje na zdroj neexistující na serveru,
* `RequestRateExceededException` - v případě, kdy byl překročen povolený počet denních dotazů; metoda `RequestRateExceededException::getMessage()` uvede čas v sekundách do dalšího možného dotazu,
* `ServerErrorException` - bude uveden v případě chyby aplikace serveru API.

### Příklady použití ###
**Iterace po souborech dokumentů produktu::**

```
#!php
<?php

$product = $client->getProduct(5924);

/* @var $docGroup \KontaktSimon\Ecatalog\Api\v2\Model\Product\DocumentGroup */
foreach ($product->getDocuments() as $docGroup) {
    echo $docGroup->getName(), PHP_EOL, "------------------------------------------------", PHP_EOL;
    /* @var $doc \KontaktSimon\Ecatalog\Api\v2\Model\Product\Document */
    foreach ($docGroup->getDocuments() as $doc) {
        echo $doc->getName(), PHP_EOL;
        /* @var $file \KontaktSimon\Ecatalog\Api\v2\Model\Product\File*/
        foreach ($doc->getFiles() as $file) {
            echo $file->getUrl(), PHP_EOL;
        }
        echo PHP_EOL;
    }
}
```

Výsledek provedení:
```
Prohlášení o shodě
------------------------------------------------

https://ekatalog.kontakt-simon.mobi/download/310/K-S_234D.pdf

Prospekty, brožury
------------------------------------------------
Brožura Simon Basic
https://ekatalog.kontakt-simon.mobi/download/38/Broszura_Simon_Basic.pdf
```

**Iterace po parametrech produktu**

```
#!php
<?php

$product = $r->getProduct(5924);

/* @var $param \KontaktSimon\Ecatalog\Api\v2\Model\Product\Param */
echo "<table><thead><tr><td>Název</td><td>Hodnota</td><td>Volitelný popis</td></tr></thead><tbody>";
foreach ($product->getParams() as $param) {
    echo "<tr><td>{$param->getName()}</td><td>{$param->getValue()}</td><td>{$param->getDescription()}</td></tr>";
}
echo "</tbody></table>";
```
