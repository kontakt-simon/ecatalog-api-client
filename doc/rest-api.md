# Kontakt-Simon E-katalog REST Api
`wersja v1.3.3`

Api pozwala autoryzowanym użytkownikom na pobieranie danych dotyczących produktów zawartych w e-katalogu Kontakt-Simon oraz powiązanych z nimi plików. Komunikacja z api odbywa się za pomoca protokołu HTTPS. Bazowe uri dla zapytań do api to `https://ekatalog.kontakt-simon.com.pl/api/v2/`.

## Uwierzytelnianie ##
Komunikacja z API następuje tylko za pomoca protokołu HTTPS. Wszystkie zapytania do API (oprócz pobierania dokumentów dołączonych do produktów) wymagają uwierzytelnienia. Uwierzytelnianie następuje poprzez przesłanie w nagłówku HTTP zapytania klucza API, który użytkownicy otrzymują od administratora systemu. Każdy użytkownik posiada swój unikalny klucz (lub wiele kluczy dla różnych aplikacji). Klucz musi być przesłany w nagłówku o nazwie `X-ApiKey`.

## Odpowiedzi API ##
Odpowiedź na każde żądanie do API odsyłana jest w formacie JSON. W przypadku poprawnego zapytania, serwer zwraca w nagłówku kod HTTP 200 oraz rezultat zapytania:

```
#!json
{
    "id": 5924,
    "symbol": "BMG2/12",
    "ean": "5902787557621",
    "name": "Gniazdo wtyczkowe podwójne bez uziemienia beżowy 16A",
    "description": "Gniazdo wtyczkowe podwójne bez uziemienia kompletne 16A, 250V, zaciski śrubowe; beż",
    "params": {
...
}
```

W przypadku błędnego zapytania lub błędu po stronie serwera w nagłówku HTTP zwracany jest kod błędu, dane w formacie JSON zawierają informacje na temat błędu, np.:

```
#!json
{
  "status": "error",
  "code": 401,
  "message": "Invalid ApiKey"
}
```

### Kody błędów ###

* 401 - brak autoryzacji - nieprawidłowy klucz api
* 404 - nie znaleziono zasobu na serwerze
* 429 - przekroczono dzienny limit zapytań; w opisie błędu znajduje się czas do następnego możliwego zapytania
* 500 - błąd aplikacji po stronie serwera

## Metody API ##

### Pobieranie listy dostępnych języków ###

`GET catalog/languages.json`

Zwraca listę identyfikatorów dostępnych języków dla wyników zwracanych przez API. Identyfikatorów można używać przy zapytaniach o dane produktów w katalogu. Domyślnie wyniki zapytań do API są zwracane w języku polskim. Przykładowy rezultat:

```
#!json
[
    "pl-PL",
    "en-GB",
    "es-ES",
    "sk-SK",
    "de-DE"
]
```

### Pobieranie drzewa kategorii produktów ###

`GET catalog/category/tree.json`

Zwraca drzewo kategorii katalogu wraz z identyfikatorami produktów przypisanych do danej kategorii.
Drzewa kategorii oraz produkty są różne dla różnych wersji językowych. Wersję można zmienić przekazując w zapytaniu opcjonalny parametr `lang`. Domyślnym językiem jest język polski (pl-PL). Listę dostępnych identyfikatorów języków można pobrać metodą `GET catalog/languages.json`. Przykładowo zapytanie `https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/category/tree.json?lang=cs-CZ` zwróci dane dla danego produktu w języku czeskim.

Przykładowy rezultat:

```
#!json
{
    "id": 754,
    "parentId": 0,
    "name": "KONTAKT-SIMON",
    "path": [],
    "subCategories": {
        "1": {
            "id": 756,
            "parentId": 754,
            "name": "Osprzet elektroinstalacyjny",
            "path": [
                754
            ],
            "subCategories": {
                "1": {
                    "id": 757,
                    "parentId": 756,
                    "name": "Simon 54",
                    "path": [
                        754,
                        756
                    ],
                    "subCategories": {
                        "1": {
                            "id": 793,
                            "parentId": 757,
                            "name": "Ramki Simon 54 Nature (+ akcesoria do ramek)",
                            "path": [
                                754,
                                756,
                                757
                            ],
                            "subCategories": {
                                "1": {
                                    "id": 100902,
                                    "parentId": 793,
                                    "path": [
                                        754,
                                        756,
                                        757,
                                        793
                                    ],
                                    "name": "Ramki 1- krotne Nature",
                                    "articleIdList": [
                                        10688,
                                        1224,
                                        10694,
                                        10689,
                                        10690,
                                        1195,
                                        10691
                                    ]
                                }
                            }
                        }
                    }
...
```

### Pobieranie danych produktu ###

`GET catalog/article/{id[,id]}.json`

Zwraca dane katalogowe jednego lub wielu produktów identyfikowanych przez parametr `id` lub ich listę rozdzieloną przecinkami. Parametrem `id` może być unikalny identyfikator w e-katalogu, kod EAN13 produktu lub jego symbol. W przypadku wykorzystania symbolu produktu należy zamienić w nim wszystkie występujące ukośniki na znak `_`, np. dla produktu o symbolu `BMG2/12` należy wysłać zapytanie o symbol `BMG2_12`.

Przykładowe zapytania:

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/5924.json` - zwróci dane produktu o id 5924

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/5902787557621.json` - zwróci dane produktu o numerze ean 5902787557621

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/dgz1buz.01_11a.json` - zwróci dane produktu o symbolu DGZ1BUZ.01/11A

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/4629,5924,dgz1buz.01_11a.json` - zwróci dane dla 3 produktów z listy (id,id,symbol).

Maksymalna ilość zwracanych wyników dla zapytania o wiele produktów to 25.

Po przekazaniu w zapytaniu opcjonalnego parametru `lang`, dane zostaną zwrócone w podanym języku. Listę dostępnych identyfikatorów języków można pobrać metodą `GET catalog/languages.json`. Przykładowo zapytanie `https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/5924.json?lang=en-GB` zwróci dane dla danego  produktu w języku angielskim.

Przykładowy rezultat zapytania dla pojedynczego produktu:

```
#!json
{
    "id": 4631,
    "symbol": "DGZ1BUZ.01/43A",
    "ean": "5902787824228",
    "name": "Gniazdo wtyczkowe pojedyncze do wersji IP44 - bez uszczelki - klapka transparentna do ramek Nature do ramek Premium (moduł) 16A 250V, zaciski śrubowe, srebrny mat, metalizowany",
    "description": "\"Gniazdo wtyczkowe do wersji IP44 z przesł. BEZ USZCZELKI do ramek wielokr. (moduł) 16A, 250V, zaciski śrubowe, klapka transp.; srebrny mat\"",
    "price": 34.77,
    "currency": "PLN",
    "stockAvailability": 764,
    "params": [
        {
            "id": 9,
            "name": "Seria",
            "value": "Simon 54"
        },
        {
            "id": 10,
            "name": "Nazwa produktu/Rodzaj",
            "value": "gniazdo wtyczkowe pojedyncze do wersji IP44 - bez uszczelki - klapka transparentna"
        },
        {
            "id": 5,
            "name": "Kolor",
            "value": "srebrny mat, metalizowany"
        },
        {
            "id": 45,
            "name": "Typ produktu",
            "value": "moduł"
        },
        {
            "id": 6,
            "name": "Prąd znamionowy",
            "value": "16A",
            "rawValue": 16,
            "rawUnit": "A"
        },
        {
            "id": 18,
            "name": "Napięcie znamionowe",
            "value": "250V",
            "rawValue": 250,
            "rawUnit": "V"
        },
        {
            "id": 19,
            "name": "Rodzaj podłączenia / Typ zacisku",
            "value": "zaciski śrubowe"
        },
        {
            "id": 34,
            "name": "Stopień ochrony IP",
            "value": "IP44"
        },
        {
            "id": 65,
            "name": "Uwagi",
            "value": "Do uzyskania klasy bryzgoszczelności IP44 zastosować uszczelkę ramki"
        },
        {
            "id": 87,
            "name": "Nadruk/Piktogram",
            "value": "brak"
        },
        {
            "id": 39,
            "name": "Rodzaj materiału",
            "value": "tworzywo sztuczne, \nPC, \nbezhalogenowe"
        },
        {
            "id": 82,
            "name": "Zabezpieczenie powierzchni",
            "value": "lakierowanie"
        },
        {
            "id": 86,
            "name": "Wykończenie powierzchni",
            "value": "matowy"
        },
        {
            "id": 84,
            "name": "Sposób montażu [ETIM]",
            "value": "montaż podtynkowy"
        },
        {
            "id": 85,
            "name": "Sposób mocowania",
            "value": "pazurki / wkręty"
        },
        {
            "id": 73,
            "name": "Wysokość produktu",
            "value": "75mm",
            "rawValue": "75",
            "rawUnit": "mm"
        },
        {
            "id": 91,
            "name": "Szerokość produktu",
            "value": "75mm",
            "rawValue": 75,
            "rawUnit": "mm"
        },
        {
            "id": 31,
            "name": "Głębokość produktu",
            "value": "45mm",
            "rawValue": 45,
            "rawUnit": "mm"
        },
        {
            "id": 94,
            "name": "Głębokość wbudowania",
            "value": "29mm",
            "rawValue": 29,
            "rawUnit": "mm"
        },
        {
            "id": 15,
            "name": "Ilość w opakowaniu zbiorczym",
            "value": "10"
        },
        {
            "id": 13,
            "name": "Jednostka miary",
            "value": "szt."
        },
        {
            "id": 22,
            "name": "EAN opakowanie jednostkowe",
            "value": "5902787824228"
        },
        {
            "id": 227,
            "name": "Typ opak. jedn.",
            "value": "folia zgrzewana"
        },
        {
            "id": 223,
            "name": "Wys. opak. jedn.",
            "value": "16cm",
            "rawValue": 16,
            "rawUnit": "cm"
        },
        {
            "id": 12,
            "name": "Szer. opak. jedn.",
            "value": "11cm",
            "description": "Wymiar poziomy boku z etykietą lub frontem produktu",
            "rawValue": 11,
            "rawUnit": "cm"
        },
        {
            "id": 219,
            "name": "Dł. opak. jedn.",
            "value": "5cm",
            "rawValue": 5,
            "rawUnit": "cm"
        },
        {
            "id": 7,
            "name": "Waga jedn.",
            "value": "0.085kg",
            "rawValue": 0.085,
            "rawUnit": "kg"
        },
        {
            "id": 224,
            "name": "EAN13 opak.",
            "value": "5902787824228"
        },
        {
            "id": 228,
            "name": "Typ opak. zbiorczego",
            "value": "pudełko zamykane"
        },
        {
            "id": 220,
            "name": "Wys. opak. zbiorczego",
            "value": "10cm",
            "rawValue": 10,
            "rawUnit": "cm"
        },
        {
            "id": 222,
            "name": "Szer. opak. zbiorczego",
            "value": "19cm",
            "description": "Wymiar poziomy boku z etykietą",
            "rawValue": 19,
            "rawUnit": "cm"
        },
        {
            "id": 221,
            "name": "Dł. opak. zbiorczego",
            "value": "23cm",
            "rawValue": 23,
            "rawUnit": "cm"
        },
        {
            "id": 88,
            "name": "Model",
            "value": "z uziemieniem, \nbolec uziemiający"
        },
        {
            "id": 109,
            "name": "Liczba modulów",
            "value": "1"
        },
        {
            "id": 114,
            "name": "Zakres częstotliwości",
            "value": "50-60Hz",
            "rawValue": {
                "min": 50,
                "max": 60
            },
            "rawUnit": "Hz"
        },
        {
            "id": 110,
            "name": "Pokrywa gniazda",
            "value": "plakietka centralna"
        },
        {
            "id": 108,
            "name": "Liczba jednostek",
            "value": "1"
        },
        {
            "id": 111,
            "name": "Klapka uchylna",
            "value": "tak"
        },
        {
            "id": 112,
            "name": "Zabezpieczenie przed dziećmi",
            "value": "tak"
        },
        {
            "id": 113,
            "name": "Zasilanie specjalne",
            "value": "bez specjalnego zasilania"
        },
        {
            "id": 107,
            "name": "Kolor klapki",
            "value": "transparentna"
        },
        {
            "id": 122,
            "name": "Zastosowanie",
            "value": "do ramek Nature, \ndo ramek Premium"
        }
    ],
    "images": [
        {
            "url": "https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/4631/assets/img_147.png",
            "md5": "4faf97e35e8d037785683f45eacc7770"
        }
    ],
    "documents": [
        {
            "groupName": "Deklaracja zgodności",
            "groupId": 1,
            "documents": [
                {
                    "name": null,
                    "files": [
                        {
                            "url": "https://ekatalog.kontakt-simon.com.pl/download/337/K-S_263B.pdf",
                            "md5": "401e5c9d7478b863b4749887975e46f7"
                        }
                    ]
                }
            ]
        },
        {
            "groupName": "Foldery, broszury",
            "groupId": 8,
            "documents": [
                {
                    "name": "Broszura Simon 54",
                    "files": [
                        {
                            "url": "https://ekatalog.kontakt-simon.com.pl/download/40/Broszura_Simon54.pdf",
                            "md5": "401e5c9d7478b863b4749887975e46f7"
                        }
                    ]
                }
            ]
        }
    ],
    "lastModified": 1475147011,
    "canonicalUrl": "https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/4631.json"
}
```

W przypadku, gdy rezultat zawiera dane więcej niż jednego produktu, odpowiedź wygląda następująco:

```
#!json
{
    "collection":
    {
        "items":
        {
            "4629":
            {
                "id": 4629,
                "symbol": "DGZ1BUZ.01/11A",
...
            },
            "4631":
            {
                "id": 4631,
                "symbol": "DGZ1BUZ.01/43A",
...
            },
        }
    }
}
```

Pod kluczem `items` znajduje się lista znalezionych produktów. Identyfikatory produktów są zarazem kluczami na tej liście. Dane poszczególnych produktów mają dokładnie taki sam format, jak dane zwracane dla zapytania o pojedynczy produkt.

W przypadku, gdy dla danego zapytania nie został znaleziony żaden rezultat, API zwraca komunikat z nagłówkiem HTTP `404 Not found`.

### Pobieranie listy uaktualnionych produktów ###

`GET catalog/changes.json?from={timestamp}`

Zwraca listę identyfikatorów produktów które zostały zmodyfikowane lub usunięte od podanego w parametrze `from` timestamp'a.


Przykładowy rezultat zapytania:

```
#!json
{
    "updatedArticles":
    [
        4611,
        4612,
        4613,
        4614,
        4615,
        7090,
        10987,
        10988,
        10989,
        10990
    ],
    "deletedArticles":
    [
    ]
}
```


### Pobieranie obrazków produktu ###

`GET catalog/article/{id-produktu}/assets/{nazwa-pliku}`

Pobieranie obrazków produktów wymaga autoryzacji za pomocą klucza API. Url'e do zasobów znajdują się w tablicy pod kluczem `images` w danych konkretnego produktu. Zalecane jest pobieranie i zapisywanie obrazków po stronie aplikacji klienta. Oprócz url'a zasobu podany jest również jego aktualny hash md5, co powinno ułatwić aktualizowanie plików.

### Pobieranie dołączonych dokumentów produktu ###
Pod kluczem `documents` w danych produktu znajdują sie pogrupowane według typu pliki dokumentów, które zostały dołączone do produktu w e-katalogu. Pobieranie plików nie wymaga autoryzacji i zapisywania po stronie aplikacji klienta.

### Pobieranie stanów magazynowych ###

`GET catalog/stocks.json`

Zwraca listę stanów magazynowych dla produktów. Kluczem głównym jest identyfikator produktu, wartością dla klucza jest obiekt zawierający symbol produktu oraz dostępną ilość w magazynie.
Stany magazynowe aktualizowane są co godzinę.


Przykładowy rezultat zapytania:

```
#!json
{
  "4775": {
    "symbol": "DT2.01\/48",
    "stockAvailability": 141
  },
  "8645": {
    "symbol": "AA23\/11",
    "stockAvailability": 5
  },
  "8150": {
    "symbol": "DS9L.01\/11",
    "stockAvailability": 100
  }
}
```


### Limit zapytań ###

Administrator systemu może ustawić dla danego kllucza limit dzienny zapytań. Po przekroczeniu limitu API zwraca komunikat błędu z liczbą sekund, które należy odczekać do kolejnego zapytania oraz status HTTP `429 Too Many Requests`. Dzienny limit zapytań, pozostałą ilość dla klucza oraz ilość zapytań danego dnia można pobrać odpytując uri:

`GET status.json`

Przykładowy rezultat zapytania:

```
#!json
{
    "dailyLimit": 10000,
    "requestsLeft": 9999,
    "requestsToday": 511
}
```

Parametr `dailyLimit` równy 0 oznacza, że dla klucza nie ustalono dziennego limitu zapytań.


