# Kontakt-Simon E-katalog REST Api #
`verzia v1.3.3`

Api umožňuje autorizovaným používateľom sťahovanie dát týkajúcich sa produktov obsiahnutých v e-katalógu Kontakt-Simon a s nimi súvisiacich súborov. Komunikácia s api prebieha s pomocou protokolu HTTPS: Základné uri pre otázky do api je `https://ekatalog.kontakt-simon.com.pl/api/v2/`.

## Overenie ##
Komunikácia s API prebieha len s pomocou protokolu HTTPS: Všetky otázky do API (okrem sťahovania dokumentov pripojených k produktom) vyžadujú overenie. Overenie prebieha odoslaním v nadpise HTTP otázky kľúča API, ktorý používatelia dostanú od správcu systému. Každý používateľ má svoj unikátny kľúč (alebo mnoho kľúčov pre rôzne aplikácie). Kľúč musí byť zaslaný v nadpise s názvom `X-ApiKey`.

## Odpovede API ##
Odpoveď na každú otázku do API je odosielaná vo formáte JSON. V prípade správnej otázky server vracia v nadpise kód HTTTP 200 a výsledok otázky:

```
#!json
{
    "id": 5924,
    "symbol": "BMG2\/12",
    "ean": "5902787557621",
    "name": "Dvojitá zásuvka bez uzemnenia béžový 16A",
    "params": [{
...
}
```

V prípade chybnej otázky alebo chyby na strane serveru je v nadpise HTTP vrátený kód chyby, údaje vo formáte JSON obsahujú informácie o téme chyby, napr.:

```
#!json
{
  "status": "error",
  "code": 401,
  "message": "Invalid ApiKey"
}
```

### Kódy chýb ###

* 401 - bez autorizácie – nesprávny kľúč api
* 404 - zdroj nebol na serveri nájdený
* 429 - bol prekročený denný limit otázok; v popise chyby sa nachádza čas do nasledujúcej možnej otázky
* 500 - chyba aplikácie na strane serveru

## Metódy API ##

### Sťahovanie zoznamu dostupných jazykov ###

`GET catalog/languages.json`

Výsledkom je zoznam identifikovaných dostupných jazykov pre výsledky vrátené API. Identifikátory možno používať pri otázke na údaje produktov v katalógu. Vo východiskovom nastavení sú výsledky otázok do API uvádzané v poľštine. Príklad výsledku:

```
#!json
[
    "pl-PL",
    "en-GB",
    "es-ES",
    "sk-SK",
    "de-DE"
]
```

### Sťahovanie adresárového stromu kategórií výrobkov ###

`GET catalog/category/tree.json`

Výsledkom je adresárový strom katalógu spoločne s identifikátormi výrobkov priradených do danej kategórie. Adresárové stromy kategórií a výrobky sú rôzne pre rôzne jazykové verzie. Verziu možno zmeniť, pokiaľ v otázke odovzdáme voliteľný parameter `lang`. Východiskovým jazykom je poľština (pl-PL). Zoznam dostupných identifikátorov jazykov možno stiahnuť metódou GET catalog/languages.json. Napríklad otázka `https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/category/tree.json?lang=sk-SK` prinesie údaje pre daný výrobok po slovensky.

Príklad výsledku:

```
#!json
{
    "id": 754,
    "parentId": 0,
    "name": "KONTAKT-SIMON",
    "path": [],
    "subCategories": {
        "1": {
            "id": 756,
            "parentId": 754,
            "name": "Elektroinštalačný materiál",
            "path": [754],
            "subCategories": {
                "1": {
                    "id": 971,
                    "parentId": 756,
                    "name": "Simon 82 Detail \/ Nature \/ Sense",
                    "path": [754, 756],
                    "subCategories": {
                        "1": {
                            "id": 1108,
                            "parentId": 971,
                            "name": "Rámčeky Simon 82 Detail ORIGINAL (+ príslušenstvo k rámčekom)",
                            "path": [754, 756, 971],
                            "subCategories": {
                                "1": {
                                    "id": 101363,
                                    "path": [754, 756, 971, 1108],
                                    "name": "Jednoduché rámčeky Detail ORIGINAL",
                                    "articleIdList": [13356, 13364, 13360, 13400, 13404, 13368, 13408],
                                    "relatedArticles": [],
                                    "relatedFrames": [],
                                    "parentId": 1108
                                },
                                "2": {
                                    "id": 101364,
                                    "path": [754, 756, 971, 1108],
                                    "name": "Dvojité rámčeky Detail ORIGINAL",
                                    "articleIdList": [13357, 13365, 13361, 13401, 13405, 13369, 13409],
                                    "relatedArticles": [],
                                    "relatedFrames": [],
                                    "parentId": 1108
                                },
                                "3": {
                                    "id": 101365,
                                    "path": [754, 756, 971, 1108],
                                    "name": "Trojité rámčeky Detail ORIGINAL",
                                    "articleIdList": [13358, 13366, 13362, 13402, 13406, 13370, 13410],
                                    "relatedArticles": [],
                                    "relatedFrames": [],
                                    "parentId": 1108
                                }
                            }
                        },
...
```

### Sťahovanie údajov výrobku ###

`GET catalog/article/{id[,id]}.json`

Prináša katalógové údaje jedného alebo mnohých produktov identifikovaných parametrom `id` alebo ich zoznam rozdelený čiarkami. Parametrom `id` môže byť unikátny identifikátor v e-katalógu, kód EAN13 výrobku alebo jeho symbol. V prípade využitia symbolu výrobku je potrebné zmeniť v ňom všetky vyskytujúce sa lomky na znak `_`, napr. pre výrobok so symbolom `BMG2/12` je potrebné vyslať otázku so symbolom `BMG2_12`.

Príklad otázky:

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/5924.json` - prinesie údaje výrobku s id 5924

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/5902787557621.json` - prinesie údaje výrobku s číslom ean 5902787557621

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/dgz1buz.01_11a.json` - prinesie údaje výrobku so symbolom DGZ1BUZ.01/11A

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/4629,5924,dgz1buz.01_11a.json` - prinesie údaje pre 3 výrobky zo zoznamu (id,id,symbol).

Maximálny počet uvádzaných výsledkov pre otázku na viac výrobkov je 25.

Po pridaní voliteľného parametra `lang` do otázky, budú údaje uvedené v zadanom jazyku. Zoznam dostupných identifikátorov jazykov možno stiahnuť metódou `GET catalog/languages.json`. Príklad otázky `https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/5924.json?lang=en-GB` prinesie údaje pre daný výrobok v angličtine.

Príklad výsledku otázky pre jednotlivý výrobok:

```
#!json
{
    "id": 4631,
    "symbol": "DGZ1BUZ.01\/43A",
    "ean": "5902787824228",
    "name": "Jedno zásuvka s krytím IP44    - bez tesnenia - transparentná klapka pre rámčeky Nature pre rámčeky Premium (prístroj s krytom) 16A 250V, skrutkové svorky, strieborná matná",
    "description": null,
    "params": [{
        "id": 9,
        "name": "Séria",
        "value": "Simon 54"
    }, {
        "id": 10,
        "name": "Názov výrobku\/typ",
        "value": "Jedno zásuvka s krytím IP44  - bez tesnenia - transparentná klapka"
    }, {
        "id": 5,
        "name": "Farba",
        "value": "strieborná matná"
    }, {
        "id": 45,
        "name": "Druh výrobku",
        "value": "prístroj s krytom"
    }, {
        "id": 6,
        "name": "Menovitý prúd",
        "value": "16A",
        "rawValue": 16,
        "rawUnit": "A"
    }, {
        "id": 18,
        "name": "Menovité napätie",
        "value": "250V",
        "rawValue": 250,
        "rawUnit": "V"
    }, {
        "id": 19,
        "name": "Typ zapojenia \/ typ svorky",
        "value": "skrutkové svorky"
    }, {
        "id": 34,
        "name": "Stupeň ochrany IP",
        "value": "IP44"
    }, {
        "id": 65,
        "name": "Poznámky",
        "value": "* Pre získanie odolnosti IP44 treba použiť tesnenie rámčeku"
    }, {
        "id": 61,
        "name": "Trieda ETIM",
        "value": "EC000125 Zásuvka"
    }, {
        "id": 87,
        "name": "Potlač\/Piktogram",
        "value": "chýbajúci"
    }, {
        "id": 39,
        "name": "Typ materiálu",
        "value": "plastická hmota, \nPC, \nbezhalogénové"
    }, {
        "id": 82,
        "name": "Povrchová ochrana",
        "value": "lakovanie"
    }, {
        "id": 86,
        "name": "Povrchová úprava",
        "value": "matný"
    }, {
        "id": 84,
        "name": "Spôsob montáže [ETIM]",
        "value": "Montáž pod omietku"
    }, {
        "id": 85,
        "name": "Spôsob upevnenia",
        "value": "pätky \/ skrutky"
    }, {
        "id": 73,
        "name": "Výška výrobku",
        "value": "75mm",
        "rawValue": "75",
        "rawUnit": "mm"
    }, {
        "id": 91,
        "name": "Šírka výrobku",
        "value": "75mm",
        "rawValue": 75,
        "rawUnit": "mm"
    }, {
        "id": 31,
        "name": "Hĺbka výrobku",
        "value": "45mm",
        "rawValue": 45,
        "rawUnit": "mm"
    }, {
        "id": 94,
        "name": "Hĺbka vstavanie",
        "value": "29mm",
        "rawValue": 29,
        "rawUnit": "mm"
    }, {
        "id": 15,
        "name": "Počet v zbernom balení",
        "value": "10"
    }, {
        "id": 13,
        "name": "Jednotka miery",
        "value": "ks"
    }, {
        "id": 22,
        "name": "EAN jednotkové balenie",
        "value": "5902787824228"
    }, {
        "id": 227,
        "name": "Typ jednotk. bal.",
        "value": "zváraná fólia"
    }, {
        "id": 223,
        "name": "Výška jedn. bal.",
        "value": "16cm",
        "rawValue": 16,
        "rawUnit": "cm"
    }, {
        "id": 12,
        "name": "Šír. hromad. bal.",
        "value": "11cm",
        "description": "Horizontálny rozmer strany s etiketou alebo s čelom výrobku",
        "rawValue": 11,
        "rawUnit": "cm"
    }, {
        "id": 219,
        "name": "Dĺ. jed. bal.",
        "value": "5cm",
        "rawValue": 5,
        "rawUnit": "cm"
    }, {
        "id": 7,
        "name": "Jed. hmotnosť",
        "value": "0.085kg",
        "rawValue": 0.085,
        "rawUnit": "kg"
    }, {
        "id": 224,
        "name": "EAN13 bal.",
        "value": "5902787824228"
    }, {
        "id": 228,
        "name": "Typ hromad. bal.",
        "value": "uzatvárateľná škatuľa"
    }, {
        "id": 220,
        "name": "Výška hromad. balení",
        "value": "10cm",
        "rawValue": 10,
        "rawUnit": "cm"
    }, {
        "id": 222,
        "name": "Šír. hromad. balenia",
        "value": "19cm",
        "description": "Horizontálny rozmer strany s etiketou",
        "rawValue": 19,
        "rawUnit": "cm"
    }, {
        "id": 221,
        "name": "Dl. hromad. balenia",
        "value": "23cm",
        "rawValue": 23,
        "rawUnit": "cm"
    }, {
        "id": 109,
        "name": "Počet modulov",
        "value": "1"
    }, {
        "id": 88,
        "name": "Model",
        "value": "s uzemnením, \nzemniaci kolík"
    }, {
        "id": 114,
        "name": "Frekvenčný rozsah",
        "value": "50-60Hz",
        "rawValue": {
            "min": 50,
            "max": 60
        },
        "rawUnit": "Hz"
    }, {
        "id": 110,
        "name": "Kryt zásuvky",
        "value": "centrálne znaky"
    }, {
        "id": 108,
        "name": "Počet jednotiek",
        "value": "1"
    }, {
        "id": 111,
        "name": "Závesná klapka",
        "value": "áno"
    }, {
        "id": 112,
        "name": "Ochrana pred deťmi",
        "value": "áno"
    }, {
        "id": 113,
        "name": "Špeciálne napájanie",
        "value": "bez špeciálneho napájania"
    }, {
        "id": 107,
        "name": "Farba klapky",
        "value": "transparentné"
    }, {
        "id": 122,
        "name": "Použitie",
        "value": "pre rámčeky Nature, \npre rámčeky Premium"
    }],
    "images": [{
        "url": "https:\/\/ekatalog.kontakt-simon.com.pl\/api\/v2\/catalog\/article\/4631\/assets\/img_147.png",
        "md5": "4faf97e35e8d037785683f45eacc7770"
    }],
    "documents": [{
        "groupName": "Vyhlásenie o zhode",
        "groupId": 1,
        "documents": [{
            "name": null,
            "files": [{
                "url": "https:\/\/ekatalog.kontakt-simon.com.pl\/download\/1050\/K-S_263H_2019_SK.pdf",
                "md5": "fbe872ef29505f28036cc0c99d9fb1ea"
            }]
        }]
    }, {
        "groupName": "Katalóg",
        "groupId": 5,
        "documents": [{
            "name": null,
            "files": [{
                "url": "https:\/\/ekatalog.kontakt-simon.com.pl\/download\/346\/Simon_54.pdf",
                "md5": "4d73f258a0737598d9b9424f49ba4792"
            }]
        }]
    }, {
        "groupName": "3D Modely",
        "groupId": 14,
        "documents": [{
            "name": null,
            "files": [{
                "url": "https:\/\/ekatalog.kontakt-simon.com.pl\/download\/761\/DGZ1BUZ_01A.max",
                "md5": "f1b19f5757f042c60919b2f553b2ba3e"
            }]
        }]
    }],
    "lastModified": 1613704689,
    "price": 7.14,
    "currency": "EUR",
    "canonicalUrl": "https:\/\/ekatalog.kontakt-simon.com.pl\/api\/v2\/catalog\/article\/4631.json",
    "stockAvailability": 24
}
```

V prípade, keď výsledok obsahuje údaje viac než jedného výrobku, vyzerá odpoveď nasledovne:

```
#!json
{
    "collection":
    {
        "items":
        {
            "4629":
            {
                "id": 4629,
                "symbol": "DGZ1BUZ.01/11A",
...
            },
            "4631":
            {
                "id": 4631,
                "symbol": "DGZ1BUZ.01/43A",
...
            },
        }
    }
}
```

Pod kľúčom `items` sa nachádza zoznam nájdených výrobkov. Identifikátory výrobkov sú na tomto zozname zároveň kľúčmi. Údaje jednotlivých výrobkov majú úplne rovnaký formát, ako údaje uvádzané pre otázku na jednotlivý výrobok.

V prípade, keď pre danú otázku nebol nájdený žiadny výsledok, API vracia správu s nadpisom HTTP `404 Not found`.

### Sťahovanie zoznamu aktualizovaných výrobkov ###

`GET catalog/changes.json?from={timestamp}`

Prináša zoznam identifikátorov výrobkov, ktoré boli upravené alebo odstránené od uvedeného v parametri `from` timestamp'a.

Príklad výsledku otázky:

```
#!json
{
    "updatedArticles":
    [
        4611,
        4612,
        4613,
        4614,
        4615,
        7090,
        10987,
        10988,
        10989,
        10990
    ],
    "deletedArticles":
    [
    ]
}
```


### Sťahovanie obrázkov výrobku ###

`GET catalog/article/{id-produktu}/assets/{nazwa-pliku}`

Sťahovanie obrázkov výrobku vyžaduje autorizáciu s pomocou kľúča API. Url k zdrojom sa nachádza v tabuľke pod kľúčom `images` v údajoch konkrétneho výrobku. Odporúča sa sťahovanie a ukladanie obrázkov na strane aplikácie klienta. Okrem url zdroja je uvedený tiež jeho aktuálny hash md5, čo by malo uľahčovať aktualizovanie súborov.

### Sťahovanie pripojených dokumentov výrobku ###
Pod kľúčom `documents` sa v údajoch výrobku nachádzajú súbory dokumentov roztriedené podľa typu, ktoré boli pripojené k výrobku v e-katalógu. Sťahovanie súborov nevyžaduje autorizáciu a ukladanie na strane aplikácie klienta.

### Sťahovanie skladových stavov ###

`GET catalog/stocks.json`

Prináša zoznam skladových stavov pre výrobky. Hlavným kľúčom je identifikátor výrobku, hodnotou pre kľúč je objekt obsahujúci symbol výrobku a v sklade dostupné množstvo. Skladové stavy sú aktualizované každú hodinu.


Príklad výsledku otázky:

```
#!json
{
  "4775": {
    "symbol": "DT2.01\/48",
    "stockAvailability": 141
  },
  "8645": {
    "symbol": "AA23\/11",
    "stockAvailability": 5
  },
  "8150": {
    "symbol": "DS9L.01\/11",
    "stockAvailability": 100
  }
}
```


### Limit otázok ###
právca systému môže nastaviť pre daný kľúč denný limit otázok. Po prekročení limitu API prináša správu chyby s počtom sekúnd, po ktorej je potrebné počkať do ďalšej otázky a stav HTTP `429 Too Many Requests`. Denný limit otázok, zostávajúci počet pre kľúč a počet otázok daného dňa si možno stiahnuť pri pýtaní uri:

`GET status.json`

Príklad výsledku otázky:

```
#!json
{
    "dailyLimit": 10000,
    "requestsLeft": 9999,
    "requestsToday": 511
}
```

Parameter `dailyLimit` rovný 0 znamená, že pre kľúč nebol nastavený denný limit otázok.
