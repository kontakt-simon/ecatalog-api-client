# PHP client for Kontakt-Simon e-catalog REST Api  #
Package for communicating with e-catalog API written for PHP language.

## Requitrements ##
PHP version >= 5.6.0

## Installation ##
Preferred way of installing package is installing it via [Composer](http://getcomposer.org/). *Composer* is package dependency manager for PHP projects. More on using *Composer* [here](https://getcomposer.org/doc/00-intro.md).

### Adding dependencies ###
Edit *composer.json* file and add e-catalog api client dependency:

```
{
    "require" : {
        "kontakt-simon/ecatalog-api-client" : "~1.3"
    },
    "repositories" : [{
            "type" : "vcs",
            "url" : "https://bitbucket.org/kontakt-simon/ecatalog-api-client.git"
        }
    ]
}
```

After installing package with `composer.phar install` command, You could start using it in Your code after importing composer autoloader (assuming, that `vendor` is default place for installed composer packages and path is accesible for script):


```
#!php
require 'vendor/autoload.php';
```

## Using package ##
First, You should create Api client object using Your private API key as shown in example below:

```
#!php
<?php

use KontaktSimon\Ecatalog\Api\v2\RestClient;

require_once __DIR__ . "/vendor/autoload.php";

define("API_KEY", "your private api key");

$client = new RestClient(API_KEY);
```

After client initialization You may start communicating with API like this:

```
#!php
<?php

...

$productId = 5924;

try {
    $product = $client->getProduct($productId);
    var_dump($product->toArray());
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    echo $e->getMessage(), PHP_EOL;
}
```

### Setting language for results  ###
By default all results from api are being returned in Polish. You could change the default language using `RestClient::setLanguage(string $lang)` method. `RestClient::getAvailableLanguages()` method returns list of available languages.

```
#!php
<?php

...

// change language to english for all consequent returned results
$client->setLanguage("en-GB");

...
```

### Fetching product data ###
Product data could be fetched using `RestClient::getProduct($productId, boolean $raw = false)` method. As `$productId` parameter You could use unique catalog id (ie. 4408), EAN13 number (ie. 5902787822101) or product symbol ("DW1ZL.01/11" for instance). By default method returns object of class [`Product`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Product.php?at=master&fileviewer=file-view-default). You could get the raw data as JSON string by passing value `true` as `$raw` parameter in method call. Method returns `null` if product was not found in catalog.

### Fetching many products at once ###
You could use `RestClient::getProducts(array $productIdList, boolean $raw = false)` method to fetch many products at once (the limitation from API is 25 results per request). Parameter `$productIdList` should be an array of product identifiers (id, ean or symbol). `$raw` parameter controls the return type - object by default or JSON string when set to `true`. If parameter `raw` is not set or false, object of class [`Collection`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Collection.php?at=master&fileviewer=file-view-default) conttaining [`Product`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Product.php?at=master&fileviewer=file-view-default) objects is being returned. If no products were found method will return empty collection. See sample code below:


```
#!php
<?php

...

$productIdList = [5924, 5902787557614, "DW1ZL.01/11"];

try {
    $products = $client->getProducts($productIdList);
    foreach ($products as $product) {
        echo $product->getName(), PHP_EOL;
    }

    // get fetched products as
    var_dump($products->toArray());

    // get product with given id from collection and convert to array
    var_dump($products->getById(5924)->toArray());

} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}
```

### Getting product category tree ###
Using `RestClient::getCategoryTree(boolean $raw = false)` method You can get whole product category tree with product identifiers attached. Method returns object of class [`Category`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Category.php?at=master&fileviewer=file-view-default). Subcategories for each category are aggregated in collection object of `Category` objects, accessible by `Category::getSubCategories()` method. List of product identifiers in category is accessible as `array` of `int` with `Category::getArticleIdList()` method. If `$raw` parameter is set to `true`, method will return JSON string instead of collection.
Notice that category structure and available products may differ between language versions. You may change language version using  `RestClient::setLanguage(string $lang)` method.

Sample code for iterating categories (more samples at the end of document):

```
#!php
<?php

...

function printCategory(\KontaktSimon\Ecatalog\Api\v2\Model\Category $category) {
    echo str_repeat('  ', count($category->getPath())), '+ ', $category->getName(), PHP_EOL;
    if (!empty($category->getArticleIdList())) {
        echo str_repeat('  ', count($category->getPath())), '  product id list: [', join(', ', $category->getArticleIdList()), ']', PHP_EOL;
    }
    foreach ($category->getSubcategories() as $subCategory) {
        printCategory($subCategory);
    }
}

/* @var \KontaktSimon\Ecatalog\Api\v2\Model\Category $catalog */
$catalog = null;

try {
    $catalog = $client->getCategoryTree();
    printCategory($catalog);
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}

```

Result:
```
+ KONTAKT-SIMON
  + Electrical equipment
    + Simon 54
      + Simon 54 Nature frames and accessories
        + 1x Nature frames
          products: [10688, 1224, 10694, 10689, 10690, 1195, 10691, 1311, 1335, 1196, 10692, 10693, 1200, 1208, 1220, 11825, 11826, 11827, 11828, 11829, 11830]
        + 2x Nature frames
          products: [10695, 1217, 10703, 10696, 10697, 1209, 10698, 1225, 1229, 10699, 10700, 10701, 10702, 1233, 1213, 11831, 11832, 11833, 11834, 11835, 11836]
        + 3x products frames
          produkty: [10704, 1218, 10714, 10705, 10706, 10707, 10708, 1230, 1234, 10709, 10710, 10711, 10712, 10713, 1214, 11837, 11838, 11839, 11840, 11841, 11842]
        + 4x Nature frames
          products: [10715, 1391, 10724, 10716, 10717, 10718, 10719, 1235, 1215, 10720, 10721, 10722, 10723, 1219, 1239, 11843, 11844, 11845, 11846, 11847, 11848]
        + 5x Nature frames
          products: [10952, 10966, 10965, 10953, 10954, 10955, 10956, 10957, 10958, 10959, 10960, 10961, 10962, 10963, 10964, 11849, 11850, 11851, 11852, 11853, 11854]
        + IP44 seals
          products: [10685, 1265, 10686, 10687, 10951]
      + Simon 54 Premium frames and accessories
        + 1x Premium frames
          products: [4998, 4999, 5000, 12762, 5001, 5002, 5003, 5028, 12761, 5029, 5030, 5031, 5032, 5033, 5058, 5062, 5066, 5070, 5074]
...
```

### Getting information on updated / deleted products ###
It's possible to fetch list of updated and/or deleted products since given time. Pass `$timestamp` argument to `RestClient::getUpdatedProducts(integer $timestamp)` method. It will return array of products updated / deleted since this unix timestamp. There's a way to get all available product ids in catalog at this time by passing 0 as `$timestamp` parameter, so it's possible to iterate the list and fetch all products data in 25 size chunks.

```
#!php
<?php

...

try {
    $modifiedProducts = $client->getUpdatedProducts(1479550422);
    var_dump($modifiedProducts["updatedArticles"]);
    var_dump($modifiedProducts["deletedArticles"]);
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}
```

### Fetching product images ###
Product images are represented by [`File`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/41882a6d7e7236125b91355fdf81db6e701b9d03/src/KontaktSimon/Ecatalog/Api/v2/Model/Product/File.php?at=master&fileviewer=file-view-default) objects. Object of class `Product` contains collection of its images accessible by `Product::getImages()` method. It's possible to download image file with method `RestClient::fetchProductImage($imageUrl, $destPath)`. Sample code:

```
#!php
<?php

...

$productId = 5924;

try {
    $product = $client->getProduct($productId);
    // get first product image and download it to folder 'images'
    /* @var $image \KontaktSimon\Ecatalog\Api\v2\Model\Product\File */
    $image = $product->getImages()->current();
    $client->fetchProductImage($image->getUrl(), "images/{$image->getFileName()}");
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}
```

### Request limit information ###
Administrator could set request limit for any api key. It's possible to check key limits and remaining requests by calling `RestClient::getStatus()` method.

```
#!php
<?php

...

// get request limit status
var_dump($client->getStatus());

...
```

Result:

```
#!php
Array
(
    [dailyLimit] => 10000
    [requestsLeft] => 9999
    [requestsToday] => 47
)
```

### Getting stock availability ###
Stock availability can be checked by calling `RestClient::getStockAvailability()` method. Inventory data is updated every hour.

```
#!php
<?php

...

// get inventory status
var_dump($client->getStockAvailability());

...
```

Method returns array where product id is the key, product symbol and inventory is the value.

```
#!php
Array
(
    [4592] => Array
        (
            [symbol] => DPZK.01/41
            [stockAvailability] => 120
        )
...
```

### Exception handling ###
In case of communication error, invalid requets data, Api client throws one of following exceptions implementing `\KontaktSimon\Ecatalog\Api\RestClient\Exception`:

* `InvalidApiKeyException` - in case of invalid Api key
* `InvalidDataReceivedException` - malformed api data
* `NotFoundException` - object not found on server
* `RequestRateExceededException` - too many daily request; call  `RequestRateExceededException::getMessage()` to get more info,
* `ServerErrorException` - thrown on api server application error

### Use cases ###
**Iterate product attached files:**

```
#!php
<?php

$product = $client->getProduct(5924);

/* @var $docGroup \KontaktSimon\Ecatalog\Api\v2\Model\Product\DocumentGroup */
foreach ($product->getDocuments() as $docGroup) {
    echo $docGroup->getName(), PHP_EOL, "------------------------------------------------", PHP_EOL;
    /* @var $doc \KontaktSimon\Ecatalog\Api\v2\Model\Product\Document */
    foreach ($docGroup->getDocuments() as $doc) {
        echo $doc->getName(), PHP_EOL;
        /* @var $file \KontaktSimon\Ecatalog\Api\v2\Model\Product\File*/
        foreach ($doc->getFiles() as $file) {
            echo $file->getUrl(), PHP_EOL;
        }
        echo PHP_EOL;
    }
}
```
Sample result:
```
Declaration of conformity
------------------------------------------------

https://ekatalog.kontakt-simon.mobi/download/310/K-S_234D.pdf

Catalogs
------------------------------------------------
Simon Basic Catalog
https://ekatalog.kontakt-simon.mobi/download/38/Broszura_Simon_Basic.pdf
```

**Iterate product parameters**

```
#!php
<?php

$product = $r->getProduct(5924);

/* @var $param \KontaktSimon\Ecatalog\Api\v2\Model\Product\Param */
echo "<table><thead><tr><td>Parameter name</td><td>Wartość</td><td>Optional description</td></tr></thead><tbody>";
foreach ($product->getParams() as $param) {
    echo "<tr><td>{$param->getName()}</td><td>{$param->getValue()}</td><td>{$param->getDescription()}</td></tr>";
}
echo "</tbody></table>";
```
