# Kontakt-Simon E-katalog REST Api
`v1.3.3`

API allows authorized users to fetch product data and its file assets from Kontakt-Simon's e-catalog. Api communication takes place using HTTPS protocol. `https://ekatalog.kontakt-simon.com.pl/api/v2/` is a base uri for all api requests.

## Authentication ##
Communication with Api is only allowed via HTTPS protocol. All requests (except for fetching product files) require authentication.
Authentication takes place by sending API key in HTTP request header. Every user has its own key (or many keys for different applications) provided by Api administrator. The key must be present in header named `X-ApiKey` in every request.

## API response ##
Every response is being sent by api in JSON format. If the request is valid, HTTP 200 code header is returned to client with corresponding JSON data, for example:

```
#!json
{
    "id": 5924,
    "symbol": "BMG2/12",
    "ean": "5902787557621",
    "name": "Double socket outlet without earthing 16A",
    "description": "Double socket outlet without earthing 16A, beige",
    "params": {
...
}
```

In case of malformed request, api error or invalid api key, corresponding HTTP error code is being returned in status header. Also, JSON response contains information on error:

```
#!json
{
  "status": "error",
  "code": 401,
  "message": "Invalid ApiKey"
}
```

### Error response codes ###

* 401 - unauthorized request / invalid api key
* 404 - resource not found
* 429 - daily request limit exceeded; there's also information when the next request could be made in error description
* 500 - server side application error

## API Methods ##

### Getting list of available languages ###

`GET catalog/languages.json`

This request lists all language identifiers available in e-catalog API. Client could use these identifiers in requests to fetch product data in specific language. Default language for api response is Polish (pl-PL). Sample result:

```
#!json
[
    "pl-PL",
    "en-GB",
    "es-ES",
    "sk-SK",
    "de-DE"
]
```

Client may request data in specific language passing `lang` parameter in HTTP request, ie. `https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/5902787557621.json?lang=de-DE` request will return product data in German.

### Getting catalog category tree ###

`GET catalog/category/tree.json`

Request returns whole catalog category tree with product identifiers attached to categiories. Category data may differ for languages.
You could change response language passing `lang` parameter to request. For instance `https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/category/tree.json?lang=cs-CZ` request will return Czech version category data.
Product identifiers attached to category are located in `articleIdList` key. You could use these identifiers in product data fetch requests.

Sample result:

```
#!json
{
    "id": 754,
    "parentId": 0,
    "name": "KONTAKT-SIMON",
    "path": [],
    "subCategories": {
        "1": {
            "id": 756,
            "parentId": 754,
            "name": "Electrical equipment",
            "path": [
                754
            ],
            "subCategories": {
                "1": {
                    "id": 757,
                    "parentId": 756,
                    "name": "Simon 54",
                    "path": [
                        754,
                        756
                    ],
                    "subCategories": {
                        "1": {
                            "id": 793,
                            "parentId": 757,
                            "name": "Simon 54 Nature Frames and accessories",
                            "path": [
                                754,
                                756,
                                757
                            ],
                            "subCategories": {
                                "1": {
                                    "id": 100902,
                                    "parentId": 793,
                                    "path": [
                                        754,
                                        756,
                                        757,
                                        793
                                    ],
                                    "name": "1x Nature Frames",
                                    "articleIdList": [
                                        10688,
                                        1224,
                                        10694,
                                        10689,
                                        10690,
                                        1195,
                                        10691
                                    ]
                                }
                            }
                        }
                    }
...
```

### Fetching product data ###

`GET catalog/article/{id[,id]}.json`

Response to this request contains data for one or many catalog product. You could fetch requested product or products by identifier or list of ids separated by commas. Unique product catalog `id` parameter, product EAN13 code or its symbol could be used as an identiifier in request. In case of using symbol as identifier, every slash character should be replaced to `_`, for example for `BMG2/12` product symbol  request identifier will be `BMG2_12`.

Sample requests:

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/5924.json` - will return data for product with catalog id 5924

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/5902787557621.json` - will return data for product with ean 5902787557621

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/dgz1buz.01_11a.json` - will return data for product with symbol DGZ1BUZ.01/11A

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/4629,5924,dgz1buz.01_11a.json` - will fetch data for 3 different products identified by id, id and symbol.

Max number of products returned in one request is 25.

If optional query parameter `lang` is provided, data will be returned in specified language. Some products may not be available for different languages.

Example result for one product request for en-GB language identifier:

```
#!json
{
    "id": 4631,
    "symbol": "DGZ1BUZ.01/43A",
    "ean": "5902787824228",
    "name": "IP44 single socket outlet without frame seal - transparent flap for Nature frames for Premium frames (Module) 16A 250V, Screw, Silver matt, metalic",
    "description": "null",
    "price": 34.77,
    "currency": "PLN",
    "stockAvailability": 764,
"params": [
        {
            "id": 9,
            "name": "Series",
            "value": "Simon 54"
        },
        {
            "id": 10,
            "name": "Product name / Type",
            "value": "IP44 single socket outlet without frame seal - transparent flap"
        },
        {
            "id": 5,
            "name": "Color",
            "value": "Silver matt, metalic"
        },
        {
            "id": 45,
            "name": "Product type",
            "value": "Module"
        },
        {
            "id": 6,
            "name": "Nominal current",
            "value": "16A",
            "rawValue": 16,
            "rawUnit": "A"
        },
        {
            "id": 18,
            "name": "Nominal voltage",
            "value": "250V",
            "rawValue": 250,
            "rawUnit": "V"
        },
        {
            "id": 19,
            "name": "Connection type / Crimp type",
            "value": "Screw"
        },
        {
            "id": 34,
            "name": "IP degree of protection",
            "value": "IP44"
        },
        {
            "id": 65,
            "name": "Notes",
            "value": "To obtain IP44 splash-proof class an additional frame seal required"
        },
        {
            "id": 61,
            "name": "ETIM Class",
            "value": "EC000125 Socket outlet"
        },
        {
            "id": 87,
            "name": "In print/Pictogram",
            "value": "none"
        },
        {
            "id": 39,
            "name": "Type of material",
            "value": "Plastic, \nPC, \nHalogen free"
        },
        {
            "id": 82,
            "name": "Surface protection",
            "value": "varhished"
        },
        {
            "id": 86,
            "name": "Type of curface",
            "value": "matt"
        },
        {
            "id": 84,
            "name": "Mounting method[ETIM]",
            "value": "flush mounted"
        },
        {
            "id": 85,
            "name": "Fixing method",
            "value": "claws / screws"
        },
        {
            "id": 73,
            "name": "Height of product",
            "value": "75mm",
            "rawValue": "75",
            "rawUnit": "mm"
        },
        {
            "id": 91,
            "name": "Width of product",
            "value": "75mm",
            "rawValue": 75,
            "rawUnit": "mm"
        },
        {
            "id": 31,
            "name": "Depth of product",
            "value": "45mm",
            "rawValue": 45,
            "rawUnit": "mm"
        },
        {
            "id": 94,
            "name": "Installation depth",
            "value": "29mm",
            "rawValue": 29,
            "rawUnit": "mm"
        },
        {
            "id": 15,
            "name": "Outer carton quantity",
            "value": "10"
        },
        {
            "id": 13,
            "name": "Measure unit",
            "value": "Qty."
        },
        {
            "id": 22,
            "name": "Individual packaging EAN code",
            "value": "5902787824228"
        },
        {
            "id": 227,
            "name": "Pack. type",
            "value": "foil"
        },
        {
            "id": 223,
            "name": "Ind. pack. height",
            "value": "16cm",
            "rawValue": 16,
            "rawUnit": "cm"
        },
        {
            "id": 12,
            "name": "Ind. pack. width",
            "value": "11cm",
            "description": "Horizontal dimentons of product with label or front",
            "rawValue": 11,
            "rawUnit": "cm"
        },
        {
            "id": 219,
            "name": "Ind. pack. lenght",
            "value": "5cm",
            "rawValue": 5,
            "rawUnit": "cm"
        },
        {
            "id": 7,
            "name": "Indiv. weight",
            "value": "0.085kg",
            "rawValue": 0.085,
            "rawUnit": "kg"
        },
        {
            "id": 224,
            "name": "Outer EAN13",
            "value": "5902787824228"
        },
        {
            "id": 228,
            "name": "Out. type",
            "value": "cardboard box"
        },
        {
            "id": 220,
            "name": "Out. height",
            "value": "10cm",
            "rawValue": 10,
            "rawUnit": "cm"
        },
        {
            "id": 222,
            "name": "Out. width",
            "value": "19cm",
            "description": "Outer width of the side with label",
            "rawValue": 19,
            "rawUnit": "cm"
        },
        {
            "id": 221,
            "name": "Out. lengt",
            "value": "23cm",
            "rawValue": 23,
            "rawUnit": "cm"
        },
        {
            "id": 109,
            "name": "Numer of modules",
            "value": "1"
        },
        {
            "id": 88,
            "name": "Model",
            "value": "with earthing, \nPin earthing"
        },
        {
            "id": 114,
            "name": "Frequency range",
            "value": "50-60Hz",
            "rawValue": {
                "min": 50,
                "max": 60
            },
            "rawUnit": "Hz"
        },
        {
            "id": 110,
            "name": "Protective flap",
            "value": "Central badge"
        },
        {
            "id": 108,
            "name": "Number of units",
            "value": "1"
        },
        {
            "id": 111,
            "name": "Hinged flap",
            "value": "Yes"
        },
        {
            "id": 112,
            "name": "Safety shutters",
            "value": "Yes"
        },
        {
            "id": 113,
            "name": "Special power supply",
            "value": "Not as special power supply"
        },
        {
            "id": 107,
            "name": "Flap color",
            "value": "Transparent"
        },
        {
            "id": 122,
            "name": "Use",
            "value": "for Nature frames, \nfor Premium frames"
        }
    ],
    "images": [
        {
            "url": "https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/4631/assets/img_147.png",
            "md5": "4faf97e35e8d037785683f45eacc7770"
        }
    ],
    "documents": [
        {
            "groupName": "Deklaracja zgodności",
            "groupId": 1,
            "documents": [
                {
                    "name": null,
                    "files": [
                        {
                            "url": "https://ekatalog.kontakt-simon.com.pl/download/337/K-S_263B.pdf",
                            "md5": "401e5c9d7478b863b4749887975e46f7"
                        }
                    ]
                }
            ]
        },
        {
            "groupName": "Catalogs",
            "groupId": 8,
            "documents": [
                {
                    "name": "Simon 54 catalog",
                    "files": [
                        {
                            "url": "https://ekatalog.kontakt-simon.com.pl/download/40/Broszura_Simon54.pdf",
                            "md5": "401e5c9d7478b863b4749887975e46f7"
                        }
                    ]
                }
            ]
        }
    ],
    "lastModified": 1475147011,
    "canonicalUrl": "https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/4631.json"
}
```

In case when many products are returned in response:

```
#!json
{
    "collection":
    {
        "items":
        {
            "4629":
            {
                "id": 4629,
                "symbol": "DGZ1BUZ.01/11A",
...
            },
            "4631":
            {
                "id": 4631,
                "symbol": "DGZ1BUZ.01/43A",
...
            },
        }
    }
}
```

Key `items` holds list of products found. Unique product identifiers are also keys for data objects on this list. Data format for every product on the list is the same as in single product request.

If no products were found, API will return `404 Not found` HTTP header.

### Getting list of updated products ###

`GET catalog/changes.json?from={timestamp}`

Returns list of product idetifiers updated or deleted since specified time. You could pass unix timestamp in `from` query parameter.
Tip: if 0 is passed as `from` parameter, list of all product identifiers available in catalog will be returned.

Sample result:

```
#!json
{
    "updatedArticles":
    [
        4611,
        4612,
        4613,
        4614,
        4615,
        7090,
        10987,
        10988,
        10989,
        10990
    ],
    "deletedArticles":
    [
    ]
}
```


### Fetching product image ###

`GET catalog/article/{id-produktu}/assets/{filename}`

Getting product image requires api authorization with key. Asset urls are located under `images` key in product data. There's also md5 checksum provided for every url to facilitate updating files on client side. Images should be downloaded (cached) and served on client side.

### Additional product files  ###

Under `documents` key in product data, there's a list of grouped file assets attached to product in catalog. Fetching additional files does not require authentication and serving from client application side.

### Product stock availability ###

`GET catalog/stocks.json`

Returns list of products with stock availability. Product id is provided as key in the list; value object holds product symbol and availability. Stocks are updated hourly.

Sample response:

```
#!json
{
  "4775": {
    "symbol": "DT2.01\/48",
    "stockAvailability": 141
  },
  "8645": {
    "symbol": "AA23\/11",
    "stockAvailability": 5
  },
  "8150": {
    "symbol": "DS9L.01\/11",
    "stockAvailability": 100
  }
}
```


### Request limit ###

System administrator may set daily request limit for api key. When the limit is reached api will return error message containing number of seconds remaining to next possible request; also `429 Too Many Requests` HTTP status header is being sent.
It's possible to check daily limit and remaining api requests by querying uri: `GET status.json`.

Sample result:

```
#!json
{
    "dailyLimit": 10000,
    "requestsLeft": 9999,
    "requestsToday": 511
}
```

Parameter `dailyLimit` of value 0 means that there's no daily limit for given key.


