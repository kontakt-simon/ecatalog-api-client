# Klient PHP pre REST Api e-katalógu Kontakt-Simon #
Paket slúži na komunikáciu s API e-katalógu produktov Kontakt-Simon.

## Požiadavky ##
PHP vo verzii >= 5.6.0

## Inštalácia ##
Preferovaným spôsobom inštalácie paketu je inštalácia s pomocou  [Composeru](http://getcomposer.org/). *Composer* je nástroj umožňujúci ľahkú správu závislostí v projekte. Umožňuje deklarovať rôzne závislosti, ktoré projekt potrebuje, a automaticky ich inštalovať alebo aktualizovať. Viac informácií o používaní *Composeru* [nájdete tu.](https://getcomposer.org/doc/00-intro.md).

### Pridávanie závislostí ###
V súbore *composer.json* je potrebné pridať nasledujúce závislosti:

```
{
    "require" : {
        "kontakt-simon/ecatalog-api-client" : "~1.3"
    },
    "repositories" : [{
            "type" : "vcs",
            "url" : "https://bitbucket.org/kontakt-simon/ecatalog-api-client.git"
        }
    ]
}
```
Po nainštalovaní paketu príkazom `composer.phar install` pridajte do kódu súbor automaticky nahrávajúci triedy:


```
#!php
require 'vendor/autoload.php';
```

## Používanie paketu ##
Objekt klient vytvorte nižšie predvedeným spôsobom, použite pritom súkromný kľúč API.

```
#!php
<?php

use KontaktSimon\Ecatalog\Api\v2\RestClient;

require_once __DIR__ . "/vendor/autoload.php";

define("API_KEY", "private api key");

$client = new RestClient(API_KEY);
```

Po iniciovaní klienta možno s jeho pomocou zasielať otázky do API e-katalógu. Nižšie príklad použitia:

```
#!php
<?php

...

$productId = 5924;

try {
    $product = $client->getProduct($productId);
    var_dump($product->toArray());
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    echo $e->getMessage(), PHP_EOL;
}
```

### Sťahovanie údajov produktu ###
Údaje jedného produktu možno stiahnuť s pomocou metódy `RestClient::getProduct($productId, boolean $raw = false)`, kde parameter `$productId` môže byť identifikátor produktu v e-katalógu (napr. 4408), číslo EAN13 (napr. 5902787822101) alebo symbol produktu (napr. "DW1ZL.01/11"). Vo východiskovom nastavení metóda upozorňuje na objekt triedy [`Product`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Product.php?at=master&fileviewer=file-view-default). Pokiaľ bude ako parameter `$raw` odovzdaná hodnota `true`, metóda ako výsledok prinesie originálne údaje vo formáte json. V prípade, keď produkt nebude nájdený, metóda uvedie výsledok `null`.

### Sťahovanie údajov mnohých produktov naraz ###
Na sťahovanie údajov mnohých produktov slúži metóda `RestClient::getProducts(array $productIdList, boolean $raw = false)`. Parameter `$productIdList` je tabuľka identifikátorov produktov (id, ean alebo symbol). Parameter `$raw` určuje, či údaje majú byť zasielané späť ako json alebo ako objekt kolekcie. V dôsledku metóda ako výsledok uvádza objekt zbierku objektov [`Collection`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Collection.php?at=master&fileviewer=file-view-default) triedy [`Product`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Product.php?at=master&fileviewer=file-view-default). V prípade, kedy nebudú nájdené žiadne produkty, metóda ako výsledok prinesie prázdnu kolekciu. Príklad kódu:


```
#!php
<?php

...

$productIdList = [5924, 5902787557614, "DW1ZL.01/11"];

try {
    $products = $client->getProducts($productIdList);
    foreach ($products as $product) {
        echo $product->getName(), PHP_EOL;
    }

    // zobrazuje celú kolekciu objektov ako tabuľku
    var_dump($products->toArray());

    // vyberá z kolekcie produkt s určitým id
    var_dump($products->getById(5924)->toArray());

} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}
```

Celý adresárový strom spoločne s priradenými produktmi možno stiahnuť s pomocou metódy `RestClient::getCategoryTree(boolean $raw = false)`. Metóda ako výsledok prináša objekt triedy [`Category`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Category.php?at=master&fileviewer=file-view-default). Podkategórie danej kategórie sa nachádzajú v kolekcii objektov triedy [`Category`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/5da3e69883f0c6db176e6a2f6c758cacd7a546d6/src/KontaktSimon/Ecatalog/Api/v2/Model/Category.php?at=master&fileviewer=file-view-default), možno ich stiahnuť s pomocou metódy `Category::getSubCategories()`. Zoznam identifikátorov produktov priradených k danej kategórii v podobe tabuľky int je dostupný s pomocou metódy `Category::getArticleIdList()`. Pokiaľ bude ako parameter `$raw` odovzdaná hodnota `true`, metóda ako výsledok prinesie originálne údaje vo formáte json. Pozor: štruktúra kategórií a k nim priradené produkty sa môžu v rôznych jazykových verziách líšiť. Verziu sťahovaných dát možno zmeniť s pomocou metódy `RestClient::setLanguage(string $lang)`.

Príklad kódu sťahovania dát (viac v príkladoch použitia na konci dokumentu):

```
#!php
<?php

...

function printCategory(\KontaktSimon\Ecatalog\Api\v2\Model\Category $category) {
    echo str_repeat('  ', count($category->getPath())), '+ ', $category->getName(), PHP_EOL;
    if (!empty($category->getArticleIdList())) {
        echo str_repeat('  ', count($category->getPath())), '  produkty: [', join(', ', $category->getArticleIdList()), ']', PHP_EOL;
    }
    foreach ($category->getSubcategories() as $subCategory) {
        printCategory($subCategory);
    }
}

/* @var \KontaktSimon\Ecatalog\Api\v2\Model\Category $catalog */
$catalog = null;

try {
    $catalog = $client->getCategoryTree();
    printCategory($catalog);
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}

```

Výsledok vykonania:

```
+ KONTAKT-SIMON
  + Elektroinštalačný materiál
    + Simon 82 Detail \/ Nature \/ Sense
      + Rámčeky Simon 82 Detail ORIGINAL (+ príslušenstvo k rámčekom)
        + Jednoduché rámčeky Detail ORIGINAL
          produkty: [13356, 13364, 13360, 13400, 13404, 13368, 13408, 13412, 13416, 13420]
        + Dvojité rámčeky Detail ORIGINAL
          produkty: [13357, 13365, 13361, 13401, 13405, 13369, 13409, 13413, 13417, 13421, 13373, 13377]
        + Trojité rámčeky Detail ORIGINAL
          produkty: [13358, 13366, 13362, 13402, 13406, 13370, 13410, 13414,]
        + Štvorité rámčeky Detail ORIGINAL
          produkty: [13359, 13367, 13363, 13403, 13407, 13371, 13411, 13415, 13419, 13423, 13375, 13379]
        + Tesnenia IP44 pod rámčeky Simon 82 Detail
          produkty: [11857, 11858, 11859, 11860]
...
```

### Sťahovanie informácií o zmenených/odstránených produktoch ###
Na sťahovanie dát o zmenených alebo odstránených produktoch slúži metóda `RestClient::getUpdatedProducts(integer $timestamp)`. Parameter `$timestamp` určuje čas, od kedy majú byť zmeny nachádzané ako výsledok. Metóda ako výsledok prináša tabuľku so zoznamom zmenených a odstránených identifikátorov produktov. Príklad kódu:

```
#!php
<?php

...

try {
    $modifiedProducts = $client->getUpdatedProducts(1479550422);
    var_dump($modifiedProducts["updatedArticles"]);
    var_dump($modifiedProducts["deletedArticles"]);
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}
```

### Sťahovanie súboru obrázok produktu ###
Údaje grafických súborov daného produktu reprezentujú objekty typu [`File`](https://bitbucket.org/kontakt-simon/ecatalog-api-client/src/41882a6d7e7236125b91355fdf81db6e701b9d03/src/KontaktSimon/Ecatalog/Api/v2/Model/Product/File.php?at=master&fileviewer=file-view-default). Objekt triedy `Product` obsahuje kolekciu dostupných grafických súborov, ktoré možno stiahnuť metódou `Product::getImages()`. Jednotlivý grafický súbor možno stiahnuť s pomocou metódy `RestClient::fetchProductImage($imageUrl, $destPath)`. Príklad kódu:

```
#!php
<?php

...

$productId = 5924;

try {
    $product = $client->getProduct($productId);
    // stiahnuť prvý obrázok pre produkt a uložiť ho v katalógu images
    /* @var $image \KontaktSimon\Ecatalog\Api\v2\Model\Product\File */
    $image = $product->getImages()->current();
    $client->fetchProductImage($image->getUrl(), "images/{$image->getFileName()}");
} catch (\KontaktSimon\Ecatalog\Api\RestClient\Exception $e) {
    die($e->getMessage());
}
```

### Zmena jazyka uvádzania výsledkov ###
Východiskové výsledky sú uvádzané v poľštine. Jazyk možno zmeniť s pomocou metódy `RestClient::setLanguage(string $lang)`. Zoznam dostupných jazykov možno stiahnuť s pomocou metódy `RestClient::getAvailableLanguages()`.

```
#!php
<?php

...

// zmeniť východiskový jazyk na angličtinu
$client->setLanguage("en-GB");

...
```

### Sťahovanie informácií o limitoch ###
Na kľúč API môže byť uvalený denný limit otázok. Stav počtu otázok možno skontrolovať vyvolaním metódy `RestClient::getStatus()`.

```
#!php
<?php

...

// stiahnuť údaje o limite otázok
var_dump($client->getStatus());

...
```

Vo výsledku získame:

```
#!php
Array
(
    [dailyLimit] => 10000
    [requestsLeft] => 9999
    [requestsToday] => 47
)
```

### Sťahovanie informácií o stave skladových zásob ###
Skladové stavy produktov možno overovať vyvolaním metódy `RestClient::getStockAvailability()`. Skladové stavy sú aktualizované každú hodinu.

```
#!php
<?php

...

// stiahnuť údaje o skladových stavoch
var_dump($client->getStockAvailability());

...
```

Vo výsledku získame tabuľku prvkov, v ktorej je kľúčom identifikátor produktu, hodnotou je potom pre daný identifikátor symbol produktu a dostupné množstvo:

```
#!php
Array
(
    [4592] => Array
        (
            [symbol] => DPZK.01/41
            [stockAvailability] => 120
        )
...
```

### Obsluha výnimiek ###
V prípade chyby komunikácie s Api alebo uvedenia nesprávnych údajov klient vypustí jednu z výnimiek implementujúcich rozhranie `\KontaktSimon\Ecatalog\Api\RestClient\Exception`:

* `InvalidApiKeyException` -  v prípade, keď je súkromný kľúč API neplatný alebo chybný,
* `InvalidDataReceivedException` - v prípade, keď klient dostane chybné údaje zo serveru API,
* `NotFoundException` - v prípade, keď sa klient odkazuje na zdroj neexistujúci na serveri,
* `RequestRateExceededException` - v prípade, keď bol prekročený povolený počet denných otázok; metóda `RequestRateExceededException::getMessage()` uvedie čas v sekundách do ďalšej možnej otázky,
* `ServerErrorException` - bude uvedený v prípade chyby aplikácie serveru API.

### Príklady použitia ###
**Iterácia po súboroch dokumentov produktu:**

```
#!php
<?php

$product = $client->getProduct(5924);

/* @var $docGroup \KontaktSimon\Ecatalog\Api\v2\Model\Product\DocumentGroup */
foreach ($product->getDocuments() as $docGroup) {
    echo $docGroup->getName(), PHP_EOL, "------------------------------------------------", PHP_EOL;
    /* @var $doc \KontaktSimon\Ecatalog\Api\v2\Model\Product\Document */
    foreach ($docGroup->getDocuments() as $doc) {
        echo $doc->getName(), PHP_EOL;
        /* @var $file \KontaktSimon\Ecatalog\Api\v2\Model\Product\File*/
        foreach ($doc->getFiles() as $file) {
            echo $file->getUrl(), PHP_EOL;
        }
        echo PHP_EOL;
    }
}
```

Výsledok vykonania:
```
Vyhlásenie o zhode
------------------------------------------------

https://ekatalog.kontakt-simon.mobi/download/310/K-S_234D.pdf

Prospekty, brožúry
------------------------------------------------
Brožúra Simon Basic
https://ekatalog.kontakt-simon.mobi/download/38/Broszura_Simon_Basic.pdf
```

**Iterácia po parametroch produktu**

```
#!php
<?php

$product = $r->getProduct(5924);

/* @var $param \KontaktSimon\Ecatalog\Api\v2\Model\Product\Param */
echo "<table><thead><tr><td>Názov parametru</td><td>Hodnota</td><td>Voliteľný popis</td></tr></thead><tbody>";
foreach ($product->getParams() as $param) {
    echo "<tr><td>{$param->getName()}</td><td>{$param->getValue()}</td><td>{$param->getDescription()}</td></tr>";
}
echo "</tbody></table>";
```
