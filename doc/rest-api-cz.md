# Kontakt-Simon E-katalog REST Api #
`verze v1.3.3`

Api umožňuje autorizovaným uživatelům stahování dat týkajících se produktů obsažených v e-katalogu Kontakt-Simon a s nimi souvisejících souborů. Komunikace s api probíhá s pomocí protokolu HTTPS: Základní uri pro dotazy do api je `https://ekatalog.kontakt-simon.com.pl/api/v2/`.

## Ověření ##
Komunikace s API probíhá jen s pomocí protokolu HTTPS: Všechny dotazy do API (kromě stahování dokumentů připojených k produktům) vyžadují ověření. Ověření probíhá odesláním v nadpisu HTTP dotazu klíče API, který uživatelé obdrží od správce systému. Každý uživatel má svůj unikátní klíč (nebo mnoho klíčů pro různé aplikace). Klíč musí být zaslán v nadpisu s názvem `X-ApiKey`.

## Odpovědi API ##
Odpověď na každý dotaz do API je odesílána ve formátu JSON. V případě správného dotazu server vrací v nadpisu kód HTTP 200 a výsledek dotazu:

```
#!json
{
    "id": 4383,
    "symbol": "DW1.01\/11",
    "ean": "5902787822002",
    "name": "Spínač jednopólový, řazení 1 (přístroj s krytem) 10AX 250V, bezšroubové, bílá",
    "params": {
...
}
```

V případě chybného dotazu nebo chyby na straně serveru je v nadpisu HTTP vrácen kód chyby, údaje ve formátu JSON obsahují informace o tématu chyby, např.:

```
#!json
{
  "status": "error",
  "code": 401,
  "message": "Invalid ApiKey"
}
```

### Kódy chyb ###

* 401 - bez autorizace - nesprávný klíč api
* 404 - zdroj nebyl na serveru nalezen
* 429 - byl překročen denní limit dotazů; v popisu chyby se nachází čas do následujícího možného dotazu
* 500 - chyba aplikace na straně serveru

## Metody API ##

### Stahování seznamu dostupných jazyků ###

`GET catalog/languages.json`

Výsledkem je seznam identifikovaných dostupných jazyků pro výsledky vracené API. Identifikátory lze používat při dotazu na údaje produktů v katalogu. Ve výchozím nastavení jsou výsledky dotazů do API uváděny v polštině. Příklad výsledku:

```
#!json
[
    "pl-PL",
    "en-GB",
    "es-ES",
    "sk-SK",
    "de-DE"
]
```

### Stahování adresářového stromu kategorií výrobků ###

`GET catalog/category/tree.json`

Výsledkem je adresářový strom katalogu společně s identifikátory výrobků přiřazených do dané kategorie. Adresářové stromy kategorií a výrobky jsou různé pro různé jazykové verze. Verzi lze změnit, pokud v dotazu předáme volitelný parametr `lang`. Výchozím jazykem je polština (pl-PL). Seznam dostupných identifikátorů jazyků lze stáhnout metodou `GET catalog/languages.json`. Na příklad dotaz `https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/category/tree.json?lang=cs-CZ` přinese údaje pro daný výrobek v češtině.

Příklad výsledku:

```
#!json
{
    "id": 754,
    "parentId": 0,
    "name": "KONTAKT-SIMON",
    "path": [],
    "subCategories": {
        "1": {
            "id": 756,
            "parentId": 754,
            "name": "Elektroinstalační materiál",
            "path": [754],
            "subCategories": {
                "1": {
                    "id": 971,
                    "parentId": 756,
                    "name": "Simon 82    Detail \/ Nature \/ Sense",
                    "path": [754, 756],
                    "subCategories": {
                        "1": {
                            "id": 1108,
                            "parentId": 971,
                            "name": "Rámečky Simon 82 Detail ORIGINAL + příslušenství",
                            "path": [754, 756, 971],
                            "subCategories": {
                                "1": {
                                    "id": 101363,
                                    "path": [754, 756, 971, 1108],
                                    "name": "Rámečky 1-násobne Detail ORIGINAL",
                                    "articleIdList": [13356, 13364, 13360, 13400, 13404, 13368, 13408, ],
                                    "relatedArticles": [],
                                    "relatedFrames": [],
                                    "parentId": 1108
                                },
                                "2": {
                                    "id": 101364,
                                    "path": [754, 756, 971, 1108],
                                    "name": "Rámečky 2-násobne Detail ORIGINAL",
                                    "articleIdList": [13357, 13365, 13361, 13401, 13405, 13369, 13409],
                                    "relatedArticles": [],
                                    "relatedFrames": [],
                                    "parentId": 1108
                                },
                                "3": {
                                    "id": 101365,
                                    "path": [754, 756, 971, 1108],
                                    "name": "Rámečky 3-násobne Detail ORIGINAL",
                                    "articleIdList": [13358, 13366, 13362, 13402, 13406, 13370, 13410],
                                    "relatedArticles": [],
                                    "relatedFrames": [],
                                    "parentId": 1108
                                },
                                "4": {
                                    "id": 101366,
                                    "path": [754, 756, 971, 1108],
                                    "name": "Rámečky 4-násobne Detail ORIGINAL",
                                    "articleIdList": [13359, 13367, 13363, 13403, 13407, ],
                                    "relatedArticles": [],
                                    "relatedFrames": [],
                                    "parentId": 1108
                                }
...
```

### Stahování údajů výrobku ###

`GET catalog/article/{id[,id]}.json`

Přináší katalogové údaje jednoho nebo mnoha produktů identifikovaných parametrem id nebo jejich seznam rozdělený čárkami. Parametrem `id` může být unikátní identifikátor v e-katalogu, kód EAN13 výrobku nebo jeho symbol. V případě využití symbolu výrobku je třeba změnit v něm všechny vyskytující se lomítka na znak `_`, např. pro výrobek se symbolem `BMG2/12` je třeba vyslat dotaz se symbolem `BMG2_12`.

Příklad dotazu:

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/5924.json` - přinese údaje výrobku s id 5924

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/5902787557621.json` - přinese údaje výrobku s číslem ean 5902787557621

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/dgz1buz.01_11a.json` - přinese údaje výrobku se symbolem DGZ1BUZ.01/11A

`https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/4629,5924,dgz1buz.01_11a.json` - přinese údaje pro 3 výrobky ze seznamu (id,id,symbol).

Maximální počet uváděných výsledků pro dotaz na více výrobků je 25.

Po přidání volitelného parametru `lang` do dotazu, budou údaje uvedeny v zadaném jazyce. Seznam dostupných identifikátorů jazyků lze stáhnout metodou `GET catalog/languages.json`. Příklad dotazu `https://ekatalog.kontakt-simon.com.pl/api/v2/catalog/article/5924.json?lang=en-GB` přinese údaje pro daný výrobek v angličtině.

Příklad výsledku dotazu pro jednotlivý výrobek:

```
#!json
{
    "id": 4631,
    "symbol": "DGZ1BUZ.01\/43A",
    "ean": "5902787824228",
    "name": "Zásuvka jednonásobná pro verzi IP44 - bez těsnění - průhledné klapka pro rámečky Nature pro rámečky Premium (přístroj s krytem) 16A 250V, šroubové svorky, stříbrná",
    "description": null,
    "params": [{
        "id": 9,
        "name": "Série",
        "value": "Simon 54"
    }, {
        "id": 10,
        "name": "Název výrobku\/Typ",
        "value": "Zásuvka jednonásobná pro verzi IP44 - bez těsnění - průhledné klapka"
    }, {
        "id": 5,
        "name": "Barva",
        "value": "stříbrná"
    }, {
        "id": 45,
        "name": "Typ výrobku",
        "value": "přístroj s krytem"
    }, {
        "id": 6,
        "name": "Jmenovitý proud",
        "value": "16A",
        "rawValue": 16,
        "rawUnit": "A"
    }, {
        "id": 18,
        "name": "Jmenovité napětí",
        "value": "250V",
        "rawValue": 250,
        "rawUnit": "V"
    }, {
        "id": 19,
        "name": "Způsob připojení \/ Typ koncovky",
        "value": "šroubové svorky"
    }, {
        "id": 34,
        "name": "Stupeň ochrany IP",
        "value": "IP44"
    }, {
        "id": 65,
        "name": "Poznámky",
        "value": "Pro provedení IP44 je nutné doplnit rámeček těsnící manžetou"
    }, {
        "id": 61,
        "name": "třída ETIM",
        "value": "EC000125 zásuvka"
    }, {
        "id": 87,
        "name": "Přetisk\/Piktogram",
        "value": "nejsou"
    }, {
        "id": 39,
        "name": "Druh materiálu",
        "value": "plastická hmota, \nPC, \nbezhalogenové"
    }, {
        "id": 82,
        "name": "Ochrana povrchu",
        "value": "lakovaný"
    }, {
        "id": 86,
        "name": "Povrchová úprava",
        "value": "matný"
    }, {
        "id": 84,
        "name": "Způsob montáže [ETIM]",
        "value": "Podomítková montáž"
    }, {
        "id": 85,
        "name": "Způsob upevnění",
        "value": "drápky\/šrouby"
    }, {
        "id": 73,
        "name": "Výška produktu",
        "value": "75mm",
        "rawValue": "75",
        "rawUnit": "mm"
    }, {
        "id": 91,
        "name": "Šířka produktu",
        "value": "75mm",
        "rawValue": 75,
        "rawUnit": "mm"
    }, {
        "id": 31,
        "name": "Hloubka produktu",
        "value": "45mm",
        "rawValue": 45,
        "rawUnit": "mm"
    }, {
        "id": 94,
        "name": "Vestavěná hloubka",
        "value": "29mm",
        "rawValue": 29,
        "rawUnit": "mm"
    }, {
        "id": 15,
        "name": "Počet v hromadném balení",
        "value": "10"
    }, {
        "id": 13,
        "name": "Měřící jednotka",
        "value": "ks"
    }, {
        "id": 22,
        "name": "EAN balení 1ks",
        "value": "5902787824228"
    }, {
        "id": 227,
        "name": "Typ. bal. 1ks",
        "value": "zatavená fólie"
    }, {
        "id": 223,
        "name": "V.bal.1ks",
        "value": "16cm",
        "rawValue": 16,
        "rawUnit": "cm"
    }, {
        "id": 12,
        "name": "Š.bal.1ks",
        "value": "11cm",
        "description": "Rozměr etikety z boku nebo čela produktu",
        "rawValue": 11,
        "rawUnit": "cm"
    }, {
        "id": 219,
        "name": "Dl.bal.1ks",
        "value": "5cm",
        "rawValue": 5,
        "rawUnit": "cm"
    }, {
        "id": 7,
        "name": "Váha 1ks",
        "value": "0.085kg",
        "rawValue": 0.085,
        "rawUnit": "kg"
    }, {
        "id": 224,
        "name": "EAN13 bal.",
        "value": "5902787824228"
    }, {
        "id": 228,
        "name": "Typ vel. bal.",
        "value": "kartonová krabice"
    }, {
        "id": 220,
        "name": "V.velkoob.bal.",
        "value": "10cm",
        "rawValue": 10,
        "rawUnit": "cm"
    }, {
        "id": 222,
        "name": "Š.velkoob.bal.",
        "value": "19cm",
        "description": "Rozměr etikety z boku produktu",
        "rawValue": 19,
        "rawUnit": "cm"
    }, {
        "id": 221,
        "name": "Dl.velkoob.bal.",
        "value": "23cm",
        "rawValue": 23,
        "rawUnit": "cm"
    }, {
        "id": 109,
        "name": "Počet modulů",
        "value": "1"
    }, {
        "id": 88,
        "name": "Model",
        "value": "s uzemněním, \nzemnící kolík"
    }, {
        "id": 114,
        "name": "Rozsah frekvence",
        "value": "50-60Hz",
        "rawValue": {
            "min": 50,
            "max": 60
        },
        "rawUnit": "Hz"
    }, {
        "id": 110,
        "name": "Kryt zásuvky",
        "value": "centrální plaketka"
    }, {
        "id": 108,
        "name": "Počet jednotek",
        "value": "1"
    }, {
        "id": 111,
        "name": "Sklápěcí klapka",
        "value": "ano"
    }, {
        "id": 112,
        "name": "Clonky proudových otvorů",
        "value": "ano"
    }, {
        "id": 113,
        "name": "Speciální napájení",
        "value": "bez speciálního napájení"
    }, {
        "id": 107,
        "name": "Barva klapky",
        "value": "transparentní"
    }, {
        "id": 122,
        "name": "Použití",
        "value": "pro rámečky Nature, \npro rámečky Premium"
    }],
    "images": [{
        "url": "https:\/\/ekatalog.kontakt-simon.com.pl\/api\/v2\/catalog\/article\/4631\/assets\/img_147.png",
        "md5": "4faf97e35e8d037785683f45eacc7770"
    }],
    "documents": [{
        "groupName": "Prohlášení o shodě",
        "groupId": 1,
        "documents": [{
            "name": null,
            "files": [{
                "url": "https:\/\/ekatalog.kontakt-simon.com.pl\/download\/496\/K-S_263H_2019_CZ.pdf",
                "md5": "e246be25eefcb8ba12c868823e2be467"
            }]
        }]
    }, {
        "groupName": "CAD výkresy",
        "groupId": 12,
        "documents": [{
            "name": null,
            "files": [{
                "url": "https:\/\/ekatalog.kontakt-simon.com.pl\/download\/738\/Simon 54 schemat elektryczny 2.dwg",
                "md5": "d104996434531c20a23035d7ebba4cc3"
            }]
        }]
    }, {
        "groupName": "3D modely",
        "groupId": 14,
        "documents": [{
            "name": null,
            "files": [{
                "url": "https:\/\/ekatalog.kontakt-simon.com.pl\/download\/761\/DGZ1BUZ_01A.max",
                "md5": "f1b19f5757f042c60919b2f553b2ba3e"
            }]
        }]
    }],
    "lastModified": 1613704689,
    "price": 186,
    "currency": "CZK",
    "canonicalUrl": "https:\/\/ekatalog.kontakt-simon.com.pl\/api\/v2\/catalog\/article\/4631.json",
    "stockAvailability": 34
}
```

V případě, kdy výsledek obsahuje údaje více než jednoho výrobku, vypadá odpověď následovně:

```
#!json
{
    "collection":
    {
        "items":
        {
            "4629":
            {
                "id": 4629,
                "symbol": "DGZ1BUZ.01/11A",
...
            },
            "4631":
            {
                "id": 4631,
                "symbol": "DGZ1BUZ.01/43A",
...
            },
        }
    }
}
```

Pod klíčem `items` se nachází seznam nalezených výrobků. Identifikátory výrobků jsou na tomto seznamu zároveň klíči. Údaje jednotlivých výrobků mají úplně stejný formát, jako údaje uváděné pro dotaz na jednotlivý výrobek.

V případě, kdy pro daný dotaz nebyl nalezen žádný výsledek, API vrací zprávu s nadpisem HTTP `404 Not found`.

### Stahování seznamu aktualizovaných výrobků ###

`GET catalog/changes.json?from={timestamp}`

Přináší seznam identifikátorů výrobků, které byly upraveny nebo odstraněny od uvedeného v parametru `from` timestamp'a.


Příklad výsledku dotazu:

```
#!json
{
    "updatedArticles":
    [
        4611,
        4612,
        4613,
        4614,
        4615,
        7090,
        10987,
        10988,
        10989,
        10990
    ],
    "deletedArticles":
    [
    ]
}
```


### Stahování obrázků výrobku ###

`GET catalog/article/{id-produktu}/assets/{nazwa-pliku}`

Stahování obrázků výrobku vyžaduje autorizaci s pomocí klíče API. Url ke zdrojům se nachází v tabulce pod klíčem `images` v údajích konkrétního výrobku. Doporučuje se stahování a ukládání obrázků na straně aplikace klienta. Kromě url zdroje je uveden také jeho aktuální hash md5, což by mělo usnadňovat aktualizování souborů.

### Stahování připojených dokumentů výrobku ###
Pod klíčem `documents` se v údajích výrobku nacházejí soubory dokumentů roztříděné podle typu, které byly připojeny k výrobku v e-katalogu. Stahování souborů nevyžaduje autorizaci a ukládání na straně aplikace klienta.

### Stahování skladových stavů ###

`GET catalog/stocks.json`

Přináší seznam skladových stavů pro výrobky. Hlavním klíčem je identifikátor výrobku, hodnotou pro klíč je objekt obsahující symbol výrobku a ve skladu dostupné množství. Skladové stavy jsou aktualizovány každou hodinu.


Příklad výsledku dotazu:

```
#!json
{
  "4775": {
    "symbol": "DT2.01\/48",
    "stockAvailability": 141
  },
  "8645": {
    "symbol": "AA23\/11",
    "stockAvailability": 5
  },
  "8150": {
    "symbol": "DS9L.01\/11",
    "stockAvailability": 100
  }
}
```

### Limit dotazů ###

Správce systému může nastavit pro daný klíč denní limit dotazů. Po překročení limitu API přináší zprávu chyby s počtem sekund, po které je třeba počkat do dalšího dotazu a stav HTTP `429 Too Many Requests`. Denní limit dotazů, zbývající počet pro klíč a počet dotazů daného dne si lze stáhnout při dotázání uri:

`GET status.json`

Příklad výsledku dotazu:

```
#!json
{
    "dailyLimit": 10000,
    "requestsLeft": 9999,
    "requestsToday": 511
}
```

Parametr `dailyLimit` rovný 0 znamená, že pro klíč nebyl nastaven denní limit dotazů.


